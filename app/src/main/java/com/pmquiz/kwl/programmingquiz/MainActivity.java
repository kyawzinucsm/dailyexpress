package com.pmquiz.kwl.programmingquiz;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.karan.churi.PermissionManager.PermissionManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

public class MainActivity extends AppCompatActivity {


   private Vibrator emptyName;
    EditText userName,userPassword;
    Button login;
    String user_name,pass;
    Boolean result;
    private ImageView logoImage;
    private SharedPreferences mSharedPreferences;
    public static final String myShared= "javaMCQ";
    public static final String myShared_userName = "name";
    PermissionManager permissionManager;
    Boolean permis =true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);



       permissionManager = new PermissionManager() { };
       permissionManager.checkAndRequestPermissions(this);
       if(permis==false){
           Log.e("permission denied", "denied");
       }

        //Custom action bar for title center and bar color
//        ActionBar bar = getSupportActionBar();
//        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#DD2D11")));
//        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
//        getSupportActionBar().setCustomView(R.layout.main_action_bar);

        //sharedprefernce for only this app Context.MODE_PRIVATE
        mSharedPreferences = getSharedPreferences(myShared, Context.MODE_PRIVATE);
        userName = (EditText) findViewById(R.id.urName);
        userPassword= (EditText) findViewById(R.id.password);
        userName.setTypeface(Typeface.SERIF);
        userPassword.setTypeface(Typeface.SERIF);
        login = (Button) findViewById(R.id.login);
        emptyName =(Vibrator) getSystemService(Context.VIBRATOR_SERVICE);


Log.e("ggg",mSharedPreferences.getString(myShared_userName,""));
        if(!mSharedPreferences.getString(myShared_userName,"").equals("")){
            startActivity(new Intent(getApplicationContext(), HomePage.class));
            finish();
        }
        login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Net_Con nc =new Net_Con();
                if(nc.isNetworkConnected(MainActivity.this)==true){
                    validateName();
                }else{
                    Toasty.error(MainActivity.this,"Please Turn on Internet",Toasty.LENGTH_LONG).show();
                }

            }
        });

    }

    public void validateName(){
        user_name= userName.getText().toString();
        pass    = userPassword.getText().toString();
        final AsyncHttpClient client = new AsyncHttpClient();
        JSONObject jsonParams = new JSONObject();
        try{
            jsonParams.put("name", user_name);
            jsonParams.put("password", pass);
            Log.e("login control ",user_name);
            HttpEntity entity = new StringEntity(jsonParams.toString(),"UTF-8");
            client.post(getApplicationContext(),"http://mobile.dailyexpress.com.mm/api/v1/login",entity,"application/json",new JsonHttpResponseHandler(){

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    // If the response is JSONObject instead of expected JSONArrayhttp://www.mobile.beedelivery.com.mm/api_v_1.0.0/req_cities.php?param=no_param
                    Log.e("in login", response.toString()+"");
                    try {
                        JSONObject serverResp = new JSONObject(response.toString());

                        if(statusCode==200){
                            result = true;
                            goToHome(result,serverResp.getString("user_id"),serverResp.getString("name"),serverResp.getString("token"));

                            //getUser(serverResp.getString("token"));
                        }else{
                            result =false;
                            goToHome(result,"","","");
                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode,Header[] headers,String response,Throwable tb){
                    result =false;
                    goToHome(result,"","","");
                    Toast.makeText(MainActivity.this, response,
                            Toast.LENGTH_LONG).show();
                }

            });

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }




    }
    public void goToHome(Boolean res,String user_id,String userName,String token){
        if(res){
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            editor.putString(myShared_userName,user_id);
            editor.putString("token",token);
            editor.putString("userName",userName);
            editor.apply();
            Intent i = new Intent(MainActivity.this, HomePage.class);
            startActivity(i);
            finish();
        }else{
            emptyName.vibrate(300);
        }
    }

    public void getUser(String token){
        AsyncHttpClient userclient = new AsyncHttpClient();
       final String token1 = token;
        userclient.addHeader("token","Bear "+token);
        userclient.get("http://mobile.dailyexpress.com.mm/api/v1/user",new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArrayhttp://www.mobile.beedelivery.com.mm/api_v_1.0.0/req_cities.php?param=no_param

                try {
                    JSONObject serverResp = new JSONObject(response.toString());

                    if(statusCode==200){
                        result = true;
                        JSONObject userdata = response.getJSONObject("data");

                        goToHome(result,userdata.getString("id"),userdata.getString("name"),token1);
                    }else{
                        result =false;
                        goToHome(result,"","","");
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode,Header[] headers,String response,Throwable tb){
                result =false;
                goToHome(result,"","","");
                Toast.makeText(MainActivity.this, "Turn on Internet ",
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
         permissionManager.checkResult(requestCode,permissions,grantResults);
        ArrayList<String> granted =permissionManager.getStatus().get(0).granted;
        ArrayList<String> denied =permissionManager.getStatus().get(0).denied;
        if(denied.size()>0){
            permis=false;
        }
    }
}
