package com.pmquiz.kwl.programmingquiz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;


public class CreateOrder extends AppCompatActivity {

    private SharedPreferences mSharedPreferences;
    EditText sender,senderPh,senderAdd,receiverName,receiverPh,receiverAdd,cod_fee,remark,item_length,item_width,item_weight,item_height,item_description,item_code,t_code;
    Spinner sender_city,receiver_city;
    RadioGroup payment;
    Button submit;
    List<City> city_list=new ArrayList<>();
    String[] spinnerArray;
    HashMap<Integer,String> spinnerMap ;
    ProgressBar pg;
    String id_sender="0";
    String townId = "";
    AsyncHttpClient client = new AsyncHttpClient();
    public void onCreate(Bundle savedInstanceState) {
        Typeface zawgyi= Typeface.createFromAsset(getAssets(),"Unicode.ttf");

        super.onCreate(savedInstanceState);
        mSharedPreferences = getSharedPreferences("javaMCQ", Context.MODE_PRIVATE);

        setContentView(R.layout.create_order);
        submit =(Button) findViewById(R.id.submit);
        Net_Con nc =new Net_Con();
        if(nc.isNetworkConnected(CreateOrder.this)==true ){

        }else{
            Toasty.error(CreateOrder.this,"Please Turn on Internet",Toasty.LENGTH_LONG).show();
        }

        pg= new ProgressBar(CreateOrder.this, null, android.R.attr.progressBarStyleSmall);
        pg.setVisibility(View.INVISIBLE);

        ActionBar bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#DD2D11")));
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.main_action_bar);
        TextView tt=(TextView) findViewById(R.id.action_bar_title);
        tt.setText("Create New Order");

        Bundle level= getIntent().getExtras();


        ScrollView mainScrollView =(ScrollView) findViewById(R.id.createScroll);
        mainScrollView.fullScroll(ScrollView.FOCUS_UP);

        sender_city=(Spinner) findViewById(R.id.sender_city) ;
        receiver_city=(Spinner) findViewById(R.id.receiver_city) ;
        getCity();



        sender_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


                String name = parent.getSelectedItem().toString();
                String Senderid = spinnerMap.get(parent.getSelectedItemPosition());
                Log.e("spinner ",name+" "+Senderid);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

    });


        //sender section
        TextInputLayout name = (TextInputLayout)findViewById(R.id.sender_layout_name);
        name.setTypeface(zawgyi);
        name.setHint("အမည်");
        TextInputLayout phNo = (TextInputLayout)findViewById(R.id.sender_layout_phone);
        phNo.setTypeface(zawgyi);
        phNo.setHint("ဖုန်းနံပါတ်");
        TextInputLayout address = (TextInputLayout)findViewById(R.id.sender_layout_address);
        address.setTypeface(zawgyi);
        address.setHint("လိပ်စာ");
        TextView scityName= (TextView) findViewById(R.id.senderCityName);
        scityName.setTypeface(zawgyi);
        scityName.setText("မြို့");
        sender=(EditText) findViewById(R.id.sender);
        senderPh =(EditText) findViewById(R.id.sender_ph);
        senderAdd=(EditText) findViewById(R.id.sender_address);
        if(level!=null){
            sender.setText(level.getString("os"));
            disableEditText(sender);
            senderPh.setText(level.getString("osPh"));
            disableEditText(senderPh);
            senderAdd.setText(level.getString("osAdd"));
            disableEditText(senderAdd);
            id_sender=level.getString("osID");
            townId = level.getString("townId");


        }
        sender.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()!=0){
                    checkEmpty();
                }else{
                    sender.setError("Please Enter sender name");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        senderPh.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()!=0){
                    checkEmpty();
                }else{
                    senderPh.setError("Please Enter sender phone number");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        senderAdd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()!=0){
                    checkEmpty();
                }else {
                    senderAdd.setError("Please Enter sender address");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });




        //receiver section
        TextInputLayout rcname = (TextInputLayout)findViewById(R.id.receiver_layout_name);
        rcname.setTypeface(zawgyi);
        rcname.setHint("အမည်");
        TextInputLayout rcphNo = (TextInputLayout)findViewById(R.id.receiver_layout_phone);
        rcphNo.setTypeface(zawgyi);
        rcphNo.setHint("ဖုန်းနံပါတ်");
        TextInputLayout rcaddress = (TextInputLayout)findViewById(R.id.receiver_layout_address);
        rcaddress.setTypeface(zawgyi);
        rcaddress.setHint("လိပ်စာ");
        TextView cityName= (TextView) findViewById(R.id.rcCityName);
        cityName.setTypeface(zawgyi);
        cityName.setText("မြို့");
        TextInputLayout idCode =(TextInputLayout) findViewById(R.id.item_code_layout);
        idCode.setTypeface(zawgyi);
        idCode.setHint("ID နံပါတ်");




        receiverName=(EditText) findViewById(R.id.receiver);
        receiverPh =(EditText) findViewById(R.id.receiver_ph);
        receiverAdd=(EditText) findViewById(R.id.receiver_address);

        receiverName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()!=0){
                    checkEmpty();
                }else {
                    receiverName.setError("Please Enter receiver name");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        receiverPh.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()!=0){
                    checkEmpty();
                }else {
                    receiverPh.setError("Please Enter receiver phone number");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        receiverAdd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()!=0){
                    checkEmpty();
                }else {
                    receiverAdd.setError("Please Enter receiver address");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        //item section
        TextInputLayout itemLength = (TextInputLayout)findViewById(R.id.item_length_layout);
        itemLength.setTypeface(zawgyi);
        itemLength.setHint("အလျား");
        TextInputLayout itemWidth = (TextInputLayout)findViewById(R.id.item_width_layout);
        itemWidth.setTypeface(zawgyi);
        itemWidth.setHint("အနံ");
        TextInputLayout itemheight = (TextInputLayout)findViewById(R.id.item_height_layout);
        itemheight.setTypeface(zawgyi);
        itemheight.setHint("အမြင့်");
        TextInputLayout itemweight= (TextInputLayout) findViewById(R.id.item_weight_layout);
        itemweight.setTypeface(zawgyi);
        itemweight.setHint("အလေးချိန်");
        TextInputLayout itemdes= (TextInputLayout) findViewById(R.id.item_desc_layout);
        itemdes.setTypeface(zawgyi);
        itemdes.setHint("ပစ္စည်းအမျိုးအစား");
        TextInputLayout itemrm= (TextInputLayout) findViewById(R.id.item_rm_layout);
        itemrm.setTypeface(zawgyi);
        itemrm.setHint("မှတ်ချက်");
        TextInputLayout cod= (TextInputLayout) findViewById(R.id.item_cod_layout);
        cod.setTypeface(zawgyi);
        cod.setHint("ငွေကောက်ရန်");
        RadioButton paid = (RadioButton) findViewById(R.id.paid);
        paid.setTypeface(zawgyi);
        paid.setText("ပေးပြီး");
        RadioButton credit = (RadioButton) findViewById(R.id.credit);
        credit.setTypeface(zawgyi);
        credit.setText("စာရင်းမှတ်");
        RadioButton collect = (RadioButton) findViewById(R.id.collect);
        collect.setTypeface(zawgyi);
        collect.setText("တောင်းရန်");
        item_length=(EditText) findViewById(R.id.item_length);
        item_width =(EditText) findViewById(R.id.item_width);
        item_height=(EditText) findViewById(R.id.item_height);
        item_weight=(EditText) findViewById(R.id.item_weight);
        item_description=(EditText) findViewById(R.id.item_desc);
        remark =(EditText) findViewById(R.id.item_rm);
        cod_fee=(EditText) findViewById(R.id.item_cod);
        payment = (RadioGroup) findViewById(R.id.payment);
        item_code=(EditText) findViewById(R.id.item_code);

        item_length.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()!=0){
                    checkEmpty();
                }else {
                    item_length.setError("Please Enter item length");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        item_width.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()!=0){
                    checkEmpty();
                }else {
                    item_width.setError("Please Enter item width");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        item_height.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()!=0){
                    checkEmpty();
                }else {
                    item_height.setError("Please Enter item height");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        item_weight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()!=0){
                    checkEmpty();
                }else {
                    item_weight.setError("Please Enter item weight");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        item_description.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()!=0){
                    checkEmpty();
                }else {
                    item_description.setError("Please Enter item description");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        cod_fee.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()!=0){
                    if(sender.getText().toString().isEmpty()||senderPh.getText().toString().isEmpty()||senderAdd.getText().toString().isEmpty()||receiverName.getText().toString().isEmpty()||receiverPh.getText().toString().isEmpty()||receiverAdd.getText().toString().isEmpty()||item_length.getText().toString().isEmpty()||item_weight.getText().toString().isEmpty()||item_width.getText().toString().isEmpty()||item_description.getText().toString().isEmpty()||item_height.getText().toString().isEmpty()||cod_fee.getText().toString().isEmpty()){
                        Toasty.error(CreateOrder.this,"Please Fill For Order",Toasty.LENGTH_LONG).show();
                    }else{
                        submit.setAlpha(1);
                        submit.setEnabled(true);
                        submit.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View view) {
                                pg.setVisibility(View.VISIBLE);
                                Net_Con nc =new Net_Con();
                                if(nc.isNetworkConnected(CreateOrder.this)==true ){
                                    submit.setAlpha((float)0.5);
                                    submit.setEnabled(false);
                                    postData();
                                }else{
                                    Toasty.error(CreateOrder.this,"Please Turn on Internet",Toasty.LENGTH_LONG).show();
                                }

                            }
                        });
                    }
                }else{
                    cod_fee.setError("Please Enter COD FEE");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                pg.setVisibility(View.VISIBLE);
                Net_Con nc =new Net_Con();
                if(nc.isNetworkConnected(CreateOrder.this)==true ){
                    submit.setAlpha((float)0.5);
                    submit.setEnabled(false);
                    postData();
                }else{
                    Toasty.error(CreateOrder.this,"Please Turn on Internet",Toasty.LENGTH_LONG).show();
                }

            }
        });


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void getCity(){
        AsyncHttpClient client = new AsyncHttpClient();

        client.addHeader("Authorization", "Bearer "+mSharedPreferences.getString("token", ""));

        client.get(getApplicationContext(),"http://mobile.dailyexpress.com.mm/api/v1/get_township_list",new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                try {
                    JSONObject serverResp = new JSONObject(response.toString());
                    Log.e("get city",serverResp.toString());
                    if(serverResp.getInt("status")==200){
                        JSONArray jsonArray=serverResp.getJSONArray("data");
                        for(int j=0;j<jsonArray.length();j++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(j);
                            City city = new City(jsonObject1.getString("township_id"),jsonObject1.getString("name"));
                            Log.e("in",city_list.size()+"");
                            city_list.add(city);
                        }
                        setDatatoSpinner(sender_city,receiver_city);
                    }else{
                        Toast.makeText(CreateOrder.this, "Token expired Please Login Again ", Toast.LENGTH_LONG).show();
                        SharedPreferences.Editor editor = mSharedPreferences.edit();
                        editor.putString("name","");
                        editor.putString("userName","");
                        editor.putString("token","");
                        editor.apply();
                        Intent i = new Intent(CreateOrder.this,MainActivity.class);
                        startActivity(i);
                        finish();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode,Header[] headers,String response,Throwable tb){


            }

        });


    }

    public void setDatatoSpinner(Spinner senderCity,Spinner receiverCity){
        spinnerArray = new String[city_list.size()];
        spinnerMap = new HashMap<Integer, String>();
        for (int i = 0; i < city_list.size(); i++)
        {
            spinnerMap.put(i,city_list.get(i).getId());
            spinnerArray[i] = city_list.get(i).getName();
            if(townId!="" && townId.equals(city_list.get(i).getId())){
                townId =i+"";
            }


        }


        ArrayAdapter<String> adapter =new ArrayAdapter<String>(getApplicationContext(),R.layout.spinner_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        if(!townId.isEmpty()&&townId!="") {
            sender_city.setEnabled(false);
            sender_city.setClickable(false);
        }
        sender_city.setAdapter(adapter);

        if(!townId.isEmpty()&& townId!=""){
//           spinnerMap.get(townId);


            sender_city.setSelection(Integer.parseInt(townId));
        }
        receiver_city.setAdapter(adapter);
    }
    public void checkEmpty(){
        if((sender.getText()!=null && sender.getText().toString().isEmpty())||(senderPh.getText()!=null && senderPh.getText().toString().isEmpty())||(senderAdd.getText()!=null && senderAdd.getText().toString().isEmpty())||(receiverName.getText()!=null && receiverName.getText().toString().isEmpty())||(receiverPh.getText()!=null && receiverPh.getText().toString().isEmpty())||(receiverAdd.getText()!=null && receiverAdd.getText().toString().isEmpty())||(item_length.getText()!=null && item_length.getText().toString().isEmpty())||(item_weight.getText()!=null && item_weight.getText().toString().isEmpty())||(item_width.getText()!=null && item_width.getText().toString().isEmpty())||(item_description.getText()!=null && item_description.getText().toString().isEmpty())||(item_height.getText()!=null && item_height.getText().toString().isEmpty())||(cod_fee.getText()!=null && cod_fee.getText().toString().isEmpty())){
            submit.setAlpha((float)0.5);
            submit.setEnabled(false);
        }else{
            submit.setAlpha(1);
            submit.setEnabled(true);
            submit.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    pg.setVisibility(View.VISIBLE);
                    Net_Con nc =new Net_Con();
                    if(nc.isNetworkConnected(CreateOrder.this)==true ){
                        submit.setAlpha((float)0.5);
                        submit.setEnabled(false);
                        postData();
                    }else{
                        Toasty.error(CreateOrder.this,"Please Turn on Internet",Toasty.LENGTH_LONG).show();
                    }

                }
            });
        }
    }

    private void disableEditText(EditText editText) {
        editText.setFocusable(false);
        editText.setEnabled(false);
        editText.setCursorVisible(false);
        editText.setKeyListener(null);
        editText.setBackgroundColor(Color.TRANSPARENT);
    }
    public void postData() {

        JSONObject jsonParams = new JSONObject();
        Log.e("idLay",id_sender+""+item_code.getText().toString());


        try {
            // Add your data

            jsonParams.put("sender_id", id_sender);
            jsonParams.put("sender", sender.getText().toString());
            jsonParams.put("senderphone", senderPh.getText().toString());
            jsonParams.put("sender_address", senderAdd.getText().toString());
            jsonParams.put("origin_id", spinnerMap.get(sender_city.getSelectedItemPosition()));

            jsonParams.put("receiver", receiverName.getText().toString());
            jsonParams.put("receiverphone", receiverPh.getText().toString());
            jsonParams.put("receiver_address", receiverAdd.getText().toString());
            jsonParams.put("destination_id", spinnerMap.get(receiver_city.getSelectedItemPosition()));

            jsonParams.put("length", item_length.getText().toString());
            jsonParams.put("width", item_width.getText().toString());
            jsonParams.put("height", item_height.getText().toString());
            jsonParams.put("remark",remark.getText().toString());
            if(item_code.getText().toString().isEmpty()){
                jsonParams.put("code","");
            }else{
                jsonParams.put("code",item_code.getText().toString());
            }

            jsonParams.put("weight",item_weight.getText().toString());
            jsonParams.put("cod_fees",cod_fee.getText().toString());

           if(payment.getCheckedRadioButtonId()==R.id.collect){
               jsonParams.put("type","2");
           }else if(payment.getCheckedRadioButtonId()==R.id.paid){
               jsonParams.put("type","0");
           }else{
               jsonParams.put("type","1");
           }


//            jsonParams.put("tracking_code",t_code.getText().toString());
            jsonParams.put("item_desc",item_description.getText().toString());

            jsonParams.put("quantity","1");


            HttpEntity entity = new StringEntity(jsonParams.toString(),"UTF-8");
            client.addHeader("Authorization", "Bearer "+mSharedPreferences.getString("token", ""));
            client.post(getApplicationContext(), "http://mobile.dailyexpress.com.mm/api/v1/req_submit_order", entity, "application/json",
                    new JsonHttpResponseHandler(){

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            // If the response is JSONObject instead of expected JSONArray
                            Log.e("order ", response.toString());
                            try {
                                JSONObject serverResp = new JSONObject(response.toString());

                                if(serverResp.getInt("status")==200){
                                    pg.setVisibility(View.GONE);
                                        JSONObject json=serverResp.getJSONObject("data");
                                        OS_result aa = new OS_result();
                                    aa.setUserName(json.getString("name"));
                                    aa.setPhoneNo(json.getString("phone"));
                                    aa.setUseraddress(json.getString("address"));

                                    aa.setTrackingCode(json.getString("qr_code"));
                                    aa.setS_Name(json.getString("sender"));
                                    aa.setS_Phone(json.getString("sender_phone"));

                                    aa.setItemCod(json.getString("cod"));
                                    aa.setItemStatus(json.getString("status"));
                                    aa.setItemAmount(json.getString("price"));
                                    aa.setDcType(json.getString("dc_type"));
                                    aa.setItemName(json.getString("item_name"));

                                    aa.setRemark_item(json.getString("remark"));
                                    aa.setItem_id(json.getString("order_id"));

                                    Intent i = new Intent(CreateOrder.this,OrderDetail.class);
                                    i.putExtra("order","");
                                    i.putExtra("order",aa);
                                    i.putExtra("createOrder","CO");

                                    startActivity(i);
                                    finish();
                                }else{
                                    Toast.makeText(CreateOrder.this, "Token expired Please Login Again ", Toast.LENGTH_LONG).show();
                                    SharedPreferences.Editor editor = mSharedPreferences.edit();
                                    editor.putString("name","");
                                    editor.putString("userName","");
                                    editor.putString("token","");
                                    editor.apply();
                                    Intent i = new Intent(CreateOrder.this,MainActivity.class);
                                    startActivity(i);
                                    finish();
                                }

                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode,Header[] headers,Throwable tb,JSONObject response){
                            Log.e("gg", response.toString());

                        }
                    });


        } catch (JSONException e) {
            // TODO Auto-generated catch block
        }
    }
}
