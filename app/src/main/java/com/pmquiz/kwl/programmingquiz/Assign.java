package com.pmquiz.kwl.programmingquiz;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

public class Assign extends AppCompatActivity {

    EditText search;
    ImageButton searchBtn;
    private SharedPreferences mSharedPreferences;
    public static final String myShared= "javaMCQ";
    String userId="";
    public static final String myShared_userName = "name";
    String state="";
    RequestParams requestParams;
    ProgressBar mprogressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.assign);
        Net_Con nc =new Net_Con();
        if(nc.isNetworkConnected(Assign.this)==true ){

        }else{
            Toasty.error(Assign.this,"Please Turn on Internet",Toasty.LENGTH_LONG).show();
        }
        //Custom action bar for title center and bar color
        ActionBar bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#DD2D11")));
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.main_action_bar);
        TextView tt=(TextView) findViewById(R.id.action_bar_title);

        mprogressBar =(ProgressBar) findViewById(R.id.simpleProgressBar);

       mprogressBar.setVisibility(View.VISIBLE);

        mSharedPreferences = getSharedPreferences(myShared, Context.MODE_PRIVATE);
        userId = mSharedPreferences.getString(myShared_userName, "");

        Intent intent = getIntent();
        state = intent.getStringExtra("itemState");

        if(state.equals("Assign")){
            tt.setText("Assign List");
        }else if(state.equals("Collected")){
            tt.setText("Collected List");
        }else if(state.equals("Delivered")){//col=1&status=delivered
            tt.setText("Delivered List");
        }else{
            tt.setText("Deliver Item List");
        }

        search =(EditText) findViewById(R.id.searchText);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mprogressBar.setVisibility(View.VISIBLE);
                    getOslist();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        searchBtn=(ImageButton) findViewById(R.id.assignBtn);
        searchBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                getOslist();
            }
        });
        getOslist();
    }
    public void getOslist() {
        //http://www.mobile.beedelivery.com.mm/api_v_1.0.0/req_orderlist.php?user=57952&col=control
        AsyncHttpClient client = new AsyncHttpClient();
        final JSONObject jsonParams = new JSONObject();
        try{
            jsonParams.put("user",userId);
            if(state.equals("Assign")){
                jsonParams.put("col","control");
            }else if(state.equals("Collected")){
                jsonParams.put("status","3");

            }else if(state.equals("Delivered")){//col=1&status=delivered
                jsonParams.put("status","7");
            }else if(state.equals("Delivering")){
                jsonParams.put("status","4");

            }

            if(search.getText()!=null && !search.getText().equals("")){
                jsonParams.put("keyword", search.getText().toString());
            }

            if(state.equals("Collected") || state.equals("Delivering")){
                jsonParams.put("keyword","");
            }
            HttpEntity entity = new StringEntity(jsonParams.toString(),"UTF-8");

            client.addHeader("Authorization", "Bearer "+mSharedPreferences.getString("token", ""));

                client.post(getApplicationContext(),"http://mobile.dailyexpress.com.mm/api/v1/req_orderlist", entity,"application/json", new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {


                        try {
                            JSONObject serverResp = new JSONObject(response.toString());

                            if (response.getInt("status")==200) {
                                mprogressBar.setVisibility(View.GONE);

                                getIntent().putExtra("assignList","");
                                getIntent().putExtra("rp",jsonParams.toString());
                                getIntent().putExtra("clickedState",state);
                                getIntent().putExtra("assignList", serverResp.getJSONArray("data").toString() );
                                FragmentManager level_fragment= getSupportFragmentManager();
                                AssignFragment osresult= new AssignFragment();
                                FrameLayout searchFrame =(FrameLayout) findViewById(R.id.assignListFrame) ;
                                searchFrame.removeAllViews();
                                level_fragment.beginTransaction().add(R.id.assignListFrame,osresult).commit();


                            } else {
                                mprogressBar.setVisibility(View.GONE);
                                Log.e("checked token",response.toString());
                                Toast.makeText(Assign.this, "Token expired Please Login Again ", Toast.LENGTH_LONG).show();
                                SharedPreferences.Editor editor = mSharedPreferences.edit();
                                editor.putString("name","");
                                editor.putString("userName","");
                                editor.putString("token","");
                                editor.apply();
                                Intent i = new Intent(Assign.this,MainActivity.class);
                                startActivity(i);
                                finish();

                            }
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String response, Throwable tb) {

                    }

                });

        }catch (JSONException e){

        }

    }

}
