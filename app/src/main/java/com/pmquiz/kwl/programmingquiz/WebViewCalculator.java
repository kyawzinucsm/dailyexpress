package com.pmquiz.kwl.programmingquiz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.entity.StringEntity;

public class WebViewCalculator extends AppCompatActivity {
    private SharedPreferences mSharedPreferences;
    public static final String myShared= "javaMCQ";

    private android.webkit.WebView wv1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calculator_webview);
        mSharedPreferences = getSharedPreferences(myShared, Context.MODE_PRIVATE);
        AsyncHttpClient client = new AsyncHttpClient();

            client.addHeader("Authorization", "Bearer "+mSharedPreferences.getString("token", ""));


                client.get(getApplicationContext(),"http://mobile.dailyexpress.com.mm/api/v1/webview", new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {



                        wv1=(WebView) findViewById(R.id.webView);
                        wv1.setWebViewClient(new MyBrowser());

                        wv1.getSettings().setLoadsImagesAutomatically(true);
                        wv1.getSettings().setJavaScriptEnabled(true);
                        wv1.setWebContentsDebuggingEnabled(true);
                        wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN)
                        {
                            enablecrossdomain41();

                            wv1.getSettings().setAllowUniversalAccessFromFileURLs(true);
                            wv1.getSettings().setAllowFileAccessFromFileURLs(true);

                        }
                        else
                        {
                            enablecrossdomain();
                        }
                        wv1.loadData(response.toString(), "text/html", "UTF-8");

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String response, Throwable tb) {
                        wv1=(WebView) findViewById(R.id.webView);
                        wv1.setWebViewClient(new MyBrowser());

                        wv1.getSettings().setLoadsImagesAutomatically(true);
                        wv1.getSettings().setJavaScriptEnabled(true);
                        wv1.setWebContentsDebuggingEnabled(true);
                        wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN)
                        {
                           // enablecrossdomain41();

                            wv1.getSettings().setAllowUniversalAccessFromFileURLs(true);
                            wv1.getSettings().setAllowFileAccessFromFileURLs(true);

                        }
                        wv1.loadData(response, "text/html", "UTF-8");
                    }

                });




    }
    public void enablecrossdomain()
    {
        try
        {
            Field field = WebView.class.getDeclaredField("mWebViewCore");
            field.setAccessible(true);
            Object webviewcore=field.get(this);
            Method method = webviewcore.getClass().getDeclaredMethod("nativeRegisterURLSchemeAsLocal", String.class);
            method.setAccessible(true);
            method.invoke(webviewcore, "http");
            method.invoke(webviewcore, "https");
        }
        catch(Exception e)
        {
            Log.d("wokao","enablecrossdomain error");
            e.printStackTrace();
        }
    }

    //for android 4.1+
    public void enablecrossdomain41()
    {
        try
        {
            Field webviewclassic_field = WebView.class.getDeclaredField("mProvider");
            webviewclassic_field.setAccessible(true);
            Object webviewclassic=webviewclassic_field.get(this);
            Field webviewcore_field = webviewclassic.getClass().getDeclaredField("mWebViewCore");
            webviewcore_field.setAccessible(true);
            Object mWebViewCore=webviewcore_field.get(webviewclassic);
            Field nativeclass_field = webviewclassic.getClass().getDeclaredField("mNativeClass");
            nativeclass_field.setAccessible(true);
            Object mNativeClass=nativeclass_field.get(webviewclassic);

            Method method = mWebViewCore.getClass().getDeclaredMethod("nativeRegisterURLSchemeAsLocal",new Class[] {int.class,String.class});
            method.setAccessible(true);
            method.invoke(mWebViewCore,mNativeClass, "http");
            method.invoke(mWebViewCore,mNativeClass, "https");
        }
        catch(Exception e)
        {
            Log.d("wokao","enablecrossdomain error");
            e.printStackTrace();
        }
    }

}

