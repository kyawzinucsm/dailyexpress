package com.pmquiz.kwl.programmingquiz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

public class BottomSheetFragment extends BottomSheetDialogFragment {

    private SharedPreferences mSharedPreferences;
    public BottomSheetFragment() {
        // Required empty public constructor

    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mSharedPreferences = getActivity().getSharedPreferences("javaMCQ", Context.MODE_PRIVATE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //Pending 0, Collected 3, Transferring 6, Received 5, Delivering 4,Delivered 7, Error 10, Cancel 11
       View view =inflater.inflate(R.layout.bottom_sheet_fragment, container, false);

        LinearLayout collectlinearLayout =(LinearLayout) view.findViewById(R.id.collectedLy);
        collectlinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendItem(mSharedPreferences.getString("oId",""),3,getContext());
                getDialog().dismiss();
            }
        });
        LinearLayout transferlinearLayout =(LinearLayout) view.findViewById(R.id.transferLy);
        transferlinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendItem(mSharedPreferences.getString("oId",""),6,getContext());
                getDialog().dismiss();
            }
        });
        LinearLayout rclinearLayout =(LinearLayout) view.findViewById(R.id.receivedLy);
        rclinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendItem(mSharedPreferences.getString("oId",""),5,getContext());
                getDialog().dismiss();
            }
        });
        LinearLayout delilinearLayout =(LinearLayout) view.findViewById(R.id.deliveringLy);
        delilinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendItem(mSharedPreferences.getString("oId",""),4,getContext());
                getDialog().dismiss();
            }
        });
        LinearLayout delivlinearLayout =(LinearLayout) view.findViewById(R.id.deliveredLy);
        delivlinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendItem(mSharedPreferences.getString("oId",""),7,getContext());
                getDialog().dismiss();
            }
        });

        LinearLayout errlinearLayout =(LinearLayout) view.findViewById(R.id.errorLy);
        errlinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendItem(mSharedPreferences.getString("oId",""),10,getContext());
                getDialog().dismiss();
            }
        });
        LinearLayout cancellinearLayout =(LinearLayout) view.findViewById(R.id.cancelLy);
        cancellinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendItem(mSharedPreferences.getString("oId",""),11,getContext());
                getDialog().dismiss();
            }
        });

       return view;
    }

    String conditionId;
    String idP;
    Context aacontext;
    public void sendItem(final String orderId, Integer val, final Context context){
         idP=orderId;
        conditionId= val.toString();
        aacontext=context;
        RequestParams rp = new RequestParams();
        String userId="" ;
        if(mSharedPreferences.contains("name")){
            userId =mSharedPreferences.getString("name","");
        }

        AsyncHttpClient client = new AsyncHttpClient();
        JSONObject jsonParams = new JSONObject();
        try{
            jsonParams.put("user",userId);jsonParams.put("pk",orderId);jsonParams.put("value",val.toString());
        }catch (JSONException e){

        }
        HttpEntity entity = new StringEntity(jsonParams.toString(),"UTF-8");
        client.addHeader("Authorization", "Bearer "+mSharedPreferences.getString("token",""));
        client.post(aacontext,"http://mobile.dailyexpress.com.mm/api/v1/req_change_stat",entity,"application/json",new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    JSONObject serverResp = new JSONObject(response.toString());
                    if(serverResp.getInt("status")==200){
                        Toasty.success(aacontext,"Successfully Updated",Toasty.LENGTH_LONG).show();
                        if(conditionId.equals("7")){
                            Intent i=new Intent(aacontext,SignActivity.class);
                            i.putExtra("idP",idP);
                            aacontext.startActivity(i);
                           // getActivity().finish();
                        }else if(conditionId.equals("10")||conditionId.equals("11")){
                            Intent i=new Intent(aacontext,RemarkActivity.class);
                            i.putExtra("msgId",idP);
                            aacontext.startActivity(i);
                           //getActivity().finish();
                        }else{

                        }
                    }else{
                        Log.e("ggg",response.toString());
                        Toast.makeText(aacontext, serverResp.getString("msg"), Toast.LENGTH_LONG).show();
                        SharedPreferences.Editor editor = mSharedPreferences.edit();
                        editor.putString("name","");
                        editor.putString("userName","");
                        editor.putString("token","");
                        editor.apply();
                        Intent i = new Intent(aacontext,MainActivity.class);
                        aacontext.startActivity(i);
                       // getActivity().finish();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode,Header[] headers,String response,Throwable tb){

                Toasty.error(aacontext, response,
                        Toast.LENGTH_LONG,true).show();
            }

        });
    }
}
