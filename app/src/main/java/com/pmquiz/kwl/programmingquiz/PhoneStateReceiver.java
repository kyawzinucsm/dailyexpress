package com.pmquiz.kwl.programmingquiz;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.provider.Telephony;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by VS00481543 on 25-10-2017.
 */

public class PhoneStateReceiver extends BroadcastReceiver {

    static final String TAG="State";
    static final String TAG1=" Inside State";
    static Boolean recordStarted;
    public static String phoneNumber;
    public static String name;
    Date startTime =new Date();
    Date endTime = new Date();
    int prevstate =0;
    private static int lastState = TelephonyManager.CALL_STATE_IDLE;

    Calendar cal=Calendar.getInstance();

    @Override
    public void onReceive(final Context context, Intent intent) {
        String stateStr = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
        int state = 0;
        if(stateStr!=null && stateStr.equals(TelephonyManager.EXTRA_STATE_IDLE)){

            state = TelephonyManager.CALL_STATE_IDLE;
        }
        else if(stateStr!=null && stateStr.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)){
            state = TelephonyManager.CALL_STATE_OFFHOOK;
        }
        else if(stateStr!=null && stateStr.equals(TelephonyManager.EXTRA_STATE_RINGING)){
            state = TelephonyManager.CALL_STATE_RINGING;
        }


        onCallStateChanged(context, state, phoneNumber);
    }


    public void onCallStateChanged(Context context,int state,String number){

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences sharedPreferences = context.getSharedPreferences("javaMCQ",Context.MODE_PRIVATE);

        Boolean switchCheckOn = pref.getBoolean("switchOn", true);
        if (switchCheckOn) {
            try {
                System.out.println("Receiver Start");
                Log.e(TAG, " onReceive: " + state);
               // Toast.makeText(context, state+" Call detected(Incoming/Outgoing) " + lastState, Toast.LENGTH_SHORT).show();

                if(lastState == state){
                    //No change, debounce extras
                    return;
                }

                    if (state == TelephonyManager.CALL_STATE_RINGING) {
                        Log.d(TAG1, " Inside " + state);
                        int j=pref.getInt("numOfCalls",0);
                        pref.edit().putInt("numOfCalls",++j).apply();
                        /* Log.d(TAG, "onReceive: num of calls "+ pref.getInt("numOfCalls",0));*/
                    } else if (state==TelephonyManager.CALL_STATE_OFFHOOK/*&& pref.getInt("numOfCalls",1)==1*/) {

                        int j = pref.getInt("numOfCalls", 0);
                        pref.edit().putInt("numOfCalls", ++j).apply();
                        Log.d(TAG, "onReceive: num of calls " + pref.getInt("numOfCalls", 0));

                        Log.d(TAG1, " recordStarted in offhook: " + recordStarted);
                        Log.d(TAG1, " Inside " + state);

                        startTime = new Date();
                        phoneNumber = sharedPreferences.getString("recordNumber", "");

                        Log.d(TAG1, " Phone Number in receiver " + phoneNumber);

                        if(phoneNumber!=null && phoneNumber!=""){
                            Log.e("in start", "stating");
                            Intent reivToServ = new Intent(context, RecorderService.class);
                            reivToServ.putExtra("number", sharedPreferences.getString("recordOrder", ""));
                            context.startService(reivToServ);

                            //name=new CommonMethods().getContactName(phoneNumber,context);

                            int serialNumber = pref.getInt("serialNumData", 1);
                            new DatabaseManager(context).addCallDetails(new CallDetails(phoneNumber, getTIme(), getDate()));

                            List<CallDetails> list = new DatabaseManager(context).getAllDetails();
                            for (CallDetails cd : list) {
                                String log = "Serial Number : " + cd.getSerial() + " | Phone num : " + cd.getNum() + " | Time : " + cd.getTime1() + " | Date : " + cd.getDate1();
                                Log.d("Database ", log);
                            }


                            //recordStarted=true;
                            pref.edit().putInt("serialNumData", ++serialNumber).apply();

                            pref.edit().putBoolean("recordStarted", true).apply();
                        }



                    } else if (state==TelephonyManager.CALL_STATE_IDLE) {

                        endTime = new Date();
                        long diff = Math.abs(endTime.getTime() - startTime.getTime());

                        long call_duration = TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(diff));
                        int k = pref.getInt("numOfCalls", 1);
                        pref.edit().putInt("numOfCalls", --k).apply();
                        int l = pref.getInt("numOfCalls", 0);
                        Log.d(TAG1, " Inside " + call_duration);
                        recordStarted = pref.getBoolean("recordStarted", false);
                        Log.d(TAG1, " recordStarted in idle :" + recordStarted+l);
                        if (recordStarted ) {
                            Log.d(TAG1, " Inside to stop recorder " + sharedPreferences.getString("recordOrder", ""));

                            context.stopService(new Intent(context, RecorderService.class));
                            AsyncHttpClient client = new AsyncHttpClient();
                            JSONObject jsonParams = new JSONObject();
                            int user_id = Integer.parseInt(sharedPreferences.getString("name", ""));
                            jsonParams.put("user_id", user_id);
                            jsonParams.put("time", call_duration);
                            jsonParams.put("phone", sharedPreferences.getString("recordNumber", ""));
                            jsonParams.put("order_id", sharedPreferences.getString("recordOrder", ""));
                            jsonParams.put("name", sharedPreferences.getString("userName", ""));
                            Log.e("sending ",jsonParams.toString());

                            HttpEntity entity = new StringEntity(jsonParams.toString(), "UTF-8");
                            client.addHeader("Authorization", "Bearer " + sharedPreferences.getString("token", ""));

                            client.post(context.getApplicationContext(), "http://mobile.dailyexpress.com.mm/api/v1/callog_save", entity, "application/json", new JsonHttpResponseHandler() {

                                @Override
                                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {


                                    try {
                                        JSONObject serverResp = new JSONObject(response.toString());
                                        Log.e("recordsavedtoServer", serverResp.toString());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                                @Override
                                public void onFailure(int statusCode, Header[] headers, String response, Throwable tb) {

                                }

                            });
                        }

                        pref.edit().putBoolean("recordStarted", false).apply();
                        sharedPreferences.edit().putString("recordNumber",null).apply();
                    }
                    lastState = state;

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
    public String getDate()
    {
        int year=cal.get(Calendar.YEAR);
        int month=cal.get(Calendar.MONTH)+1;
        int day=cal.get(Calendar.DATE);
        String date=String.valueOf(day)+"_"+String.valueOf(month)+"_"+String.valueOf(year);

        return date;
    }

    public String getTIme()
    {
        String am_pm="";
        int sec=cal.get(Calendar.SECOND);
        int min=cal.get(Calendar.MINUTE);
        int hr=cal.get(Calendar.HOUR);
        int amPm=cal.get(Calendar.AM_PM);
        if(amPm==1)
            am_pm="PM";
        else if(amPm==0)
            am_pm="AM";

        String time=String.valueOf(hr)+":"+String.valueOf(min)+":"+String.valueOf(sec)+" "+am_pm;

        return time;
    }
}