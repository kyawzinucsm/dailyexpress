package com.pmquiz.kwl.programmingquiz;

import java.io.Serializable;

public class OS_result implements Serializable {
    String userName;
    String phoneNo;
    String useraddress;
    String trackingCode;
    String s_Name;
    String s_Phone;
    String itemStatus;
    String itemAmount;
    String itemName;
    String itemCod;
    String dcType;
    String remark_item;
    String item_id;
    String check;

    public String getFormatted_date() {
        return formatted_date;
    }

    public String getCheck() {
        return check;
    }

    public void setCheck(String check) {
        this.check = check;
    }

    public void setFormatted_date(String formatted_date) {
        this.formatted_date = formatted_date;
    }

    String formatted_date;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getUseraddress() {
        return useraddress;
    }

    public void setUseraddress(String useraddress) {
        this.useraddress = useraddress;
    }

    public String getTrackingCode() {
        return trackingCode;
    }

    public void setTrackingCode(String trackingCode) {
        this.trackingCode = trackingCode;
    }

    public String getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(String itemStatus) {
        this.itemStatus = itemStatus;
    }

    public String getItemAmount() {
        return itemAmount;
    }

    public void setItemAmount(String itemAmount) {
        this.itemAmount = itemAmount;
    }

    public String getS_Name() {
        return s_Name;
    }

    public void setS_Name(String s_Name) {
        this.s_Name = s_Name;
    }

    public String getS_Phone() {
        return s_Phone;
    }

    public void setS_Phone(String s_Phone) {
        this.s_Phone = s_Phone;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemCod() {
        return itemCod;
    }

    public void setItemCod(String itemCod) {
        this.itemCod = itemCod;
    }

    public String getDcType() {
        return dcType;
    }

    public void setDcType(String dcType) {
        this.dcType = dcType;
    }

    public String getRemark_item() {
        return remark_item;
    }

    public void setRemark_item(String remark_item) {
        this.remark_item = remark_item;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }
}
