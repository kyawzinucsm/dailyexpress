package com.pmquiz.kwl.programmingquiz;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.List;

public class OSListView extends RecyclerView.Adapter<OSListView.ViewHolder>{
    private List<OS_result> osList;
    private Context mContext;
    private OnItemClickListener mOnItemClickListener;
    private CardView levelBgCard;


    public interface OnItemClickListener {
        void onClick(View view, int position);
    }

    public OSListView(List<OS_result> list, Context context) {
        this.mContext = context;
        this.osList = list;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.os_result_card, parent, false);

        return new ViewHolder(view);
    }


    public void onBindViewHolder(final ViewHolder holder, int position) {

        OS_result osResult = osList.get(position);
        Log.e("os reuslt list ",osList.size()+"");
        holder.userName.setText(osResult.getUserName());
        holder.phNo.setText(osResult.getPhoneNo());
        holder.userAdd.setText(osResult.getUseraddress());
        if(osResult.getTrackingCode()!=null){
            holder.tCode.setVisibility(View.VISIBLE);
            holder.tCode.setText(osResult.getTrackingCode());
        }
        if(osResult.getItemStatus()!=null){
            holder.tSts.setVisibility(View.VISIBLE);
            holder.tSts.setText(osResult.getItemStatus());
        }
       // holder.cV.setCardBackgroundColor(Color.parseColor("#F0ECEC"));
        holder.cv.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                mOnItemClickListener.onClick(view, holder.getAdapterPosition());
            }
        });

    }

    public int getItemCount() {
        return osList.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView userName,phNo,userAdd,tCode,tSts;
        CardView cv;


        public ViewHolder(View itemView) {
            super(itemView);
            Typeface zawgyi= Typeface.createFromAsset(itemView.getContext().getAssets(),"Unicode.ttf");
            userName = (TextView) itemView.findViewById(R.id.user);
            phNo=(TextView) itemView.findViewById(R.id.ph);
            userAdd =(TextView) itemView.findViewById(R.id.add);
            tCode =(TextView) itemView.findViewById(R.id.tCode);
            tSts =(TextView) itemView.findViewById(R.id.tSts);
            cv=(CardView) itemView.findViewById(R.id.os_result_card);
            userName.setTypeface(zawgyi);
            phNo.setTypeface(zawgyi);
            userAdd.setTypeface(zawgyi);



        }
    }
}
