package com.pmquiz.kwl.programmingquiz;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.gcacace.signaturepad.views.SignaturePad;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

public class SignActivity extends Activity {

    private SignaturePad mSignaturePad;
    private Button mClearButton,cancelButton;
    private Button mSaveButton;
    private SharedPreferences shp;
    String o_id;

    ProgressBar pg;
    AsyncHttpClient client = new AsyncHttpClient();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.sign_delivered);
        Bundle level= getIntent().getExtras();
        o_id=level.getString("idP");
        shp = getSharedPreferences("javaMCQ", Context.MODE_PRIVATE);
        pg =(ProgressBar)findViewById(R.id.signProgress);
        mSignaturePad = (SignaturePad) findViewById(R.id.signature_pad);
        mSignaturePad.setOnSignedListener(new SignaturePad.OnSignedListener() {
            @Override
            public void onStartSigning() {

            }

            @Override
            public void onSigned() {
                mSaveButton.setEnabled(true);
                mClearButton.setEnabled(true);
            }

            @Override
            public void onClear() {
                mSaveButton.setEnabled(false);
                mClearButton.setEnabled(false);
            }
        });

        mClearButton = (Button) findViewById(R.id.clear_button);
        mSaveButton = (Button) findViewById(R.id.save_button);
        cancelButton = (Button) findViewById(R.id.cancel_button);

        mClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSignaturePad.clear();
            }
        });

        mSaveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Net_Con nc = new Net_Con();
                if(nc.isNetworkConnected(SignActivity.this)==true){
                    Bitmap signatureBitmap = mSignaturePad.getSignatureBitmap();
                    saveBitmapToJPG(signatureBitmap);
                }else{
                    Toasty.error(SignActivity.this,"Please Turn on Internet",Toasty.LENGTH_LONG).show();

                }

            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Intent i = new Intent(SignActivity.this,HomePage.class);
               startActivity(i);
               finish();
            }
        });
    }



        public void saveBitmapToJPG(Bitmap bitmap)  {


        pg.setVisibility(View.VISIBLE);
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        newBitmap.compress(Bitmap.CompressFormat.PNG,80,baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
            JSONObject jsonParams = new JSONObject();
        Log.e("imageEncoded",imageEncoded);

            try {
                // Add your data

                jsonParams.put("order_id", o_id);
                jsonParams.put("image",imageEncoded);



                HttpEntity entity = new StringEntity(jsonParams.toString(),"UTF-8");
                Log.e("ggsigna", jsonParams.toString());
                client.addHeader("Authorization", "Bearer "+shp.getString("token", ""));
                client.post(getApplicationContext(), "http://mobile.dailyexpress.com.mm/api/v1//req_save_signature", entity, "application/json",
                        new JsonHttpResponseHandler(){
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                                // If the response is JSONObject instead of expected JSONArray
                                try {
                                    Log.e("gg signatue", response.toString());
                                    JSONObject serverResp = new JSONObject(response.toString());
                                    if(serverResp.getInt("status")==200){
                                        pg.setVisibility(View.GONE);
                                        Intent i = new Intent(SignActivity.this,HomePage.class);
                                        startActivity(i);
                                        finish();
                                    }else{
                                        Toast.makeText(SignActivity.this, "Token expired Please Login Again ", Toast.LENGTH_LONG).show();
                                        SharedPreferences.Editor editor = shp.edit();
                                        editor.putString("name","");
                                        editor.putString("userName","");
                                        editor.putString("token","");
                                        editor.apply();
                                        Intent i = new Intent(SignActivity.this,MainActivity.class);
                                        startActivity(i);
                                        finish();
                                    }

                                } catch (JSONException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onFailure(int statusCode,Header[] headers,String response,Throwable tb){
                                pg.setVisibility(View.GONE);
                                Log.e("ErrorSIgn", response);

                            }
                        });


            } catch (JSONException e) {
                // TODO Auto-generated catch block
            }
    }






}
