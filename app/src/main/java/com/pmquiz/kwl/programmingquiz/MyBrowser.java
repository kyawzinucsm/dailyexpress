package com.pmquiz.kwl.programmingquiz;

import android.webkit.WebView;
import android.webkit.WebViewClient;

 class MyBrowser extends WebViewClient {
     public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadData(url, "text/html", "UTF-8");
        return true;
    }
}