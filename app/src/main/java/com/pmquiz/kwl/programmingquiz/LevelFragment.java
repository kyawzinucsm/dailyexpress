package com.pmquiz.kwl.programmingquiz;


import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class LevelFragment extends Fragment {

    private final static String MIPMAP = "mipmap";

    private RecyclerView mRecycleView;
    private Activity mActivity;
    private CardView layout_card;
    BottomNavigationView bottomNavigationView;

    public LevelFragment() {

    }

    public static LevelFragment newInstance() {
        LevelFragment fragment = new LevelFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.level_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mActivity = getActivity();
        mRecycleView = (RecyclerView) view.findViewById(R.id.level_fragment);
        bottomNavigationView=(BottomNavigationView) getActivity().findViewById(R.id.bottom_navigation);
        mRecycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {



            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {

                if (dy > 0 && bottomNavigationView.isShown()) {
                    bottomNavigationView.setVisibility(View.GONE);
                } else if (dy < 0 ) {
                    bottomNavigationView.setVisibility(View.VISIBLE);

                }



            }
        });
        setLevelView();
    }

    private void setLevelView() {
        List<Level> levelList= new ArrayList<>();
        List<String> levelName =new ArrayList<String>();

        levelName.add("အခြားသို့ပို့");//0
        levelName.add("ပို့ဆောင်စဉ်");//1

        levelName.add("ပို့ဆောင်ပြီး");//2
        levelName.add("ပို့၍မရ");//3

        levelName.add("Cancel");//4
        levelName.add("Assign");//5


        levelName.add("Scan By ID");//6
        levelName.add("လက်ခံရရှိ");//7
        levelName.add("ပါဆယ်လက်ခံ");//8
        for(int j=0;j<levelName.size();j++){
            Level level =new Level();
            level.setLevelname(levelName.get(j));
            levelList.add(level);
        }
        loadData(levelList);
        LevelAdapterView levelAdapterView = new LevelAdapterView(levelList, getContext());
        setOnclickListener(levelAdapterView, levelList);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mActivity, 2);
        mRecycleView.setLayoutManager(gridLayoutManager);
        mRecycleView.setAdapter(levelAdapterView);
    }

    private void loadData(List<Level> levelList) {
        Resources resources = mActivity.getResources();
        String packageName = mActivity.getPackageName();
        for(Level level:levelList) {
            String iconName = level.getLevelname().toLowerCase();//icon = getResourceByName(iconName,"drawable", resources, packageName);
            int icon=0;
            if(level.getLevelname().toLowerCase().replaceAll("\\s+","").equals("scanbyid")){
                icon = getResourceByName("id","drawable", resources, packageName);

            }else if(level.getLevelname().toLowerCase().replaceAll("\\s+","").equals("ပါဆယ်လက်ခံ")){
                icon = getResourceByName("collected","drawable", resources, packageName);
            }else if(level.getLevelname().toLowerCase().replaceAll("\\s+","").equals("ပို့ဆောင်စဉ်")){
                icon = getResourceByName("delivering","drawable", resources, packageName);
            }else if(level.getLevelname().toLowerCase().replaceAll("\\s+","").equals("ပို့ဆောင်ပြီး")){
                icon = getResourceByName("delivered","drawable", resources, packageName);
            }else if(level.getLevelname().toLowerCase().replaceAll("\\s+","").equals("ပို့၍မရ")){
                icon = getResourceByName("error","drawable", resources, packageName);
            }else if(level.getLevelname().toLowerCase().replaceAll("\\s+","").equals("အခြားသို့ပို့")){
                icon = getResourceByName("transferring","drawable", resources, packageName);
            }else if(level.getLevelname().toLowerCase().replaceAll("\\s+","").equals("လက်ခံရရှိ")){
                icon = getResourceByName("received","drawable", resources, packageName);
            }else{
                icon = getResourceByName(iconName,"drawable", resources, packageName);
            }
            level.setIcon_id(icon);
        }
    }

    private int getResourceByName(String name, String identifier, Resources resources, String packageName) {
        return resources.getIdentifier(name, identifier, packageName);
    }

    private void setOnclickListener(final LevelAdapterView levelAdapterView, final List<Level> levelList) {
        levelAdapterView.setOnItemClickListener(new LevelAdapterView.OnItemClickListener() {

            @Override
            public void onClick(View view, int position) {
                Intent i ;
                //Collected+Delivering, Delivered+Error, Cancel+ Assign, Transferring+Received, Scan by ID
                //Pending 0, Collected 3, Transferring 6, Received 5, Delivering 4,Delivered 7, Error 10, Cancel 11
                if(levelList.get(position).getLevelname().toLowerCase().replaceAll("\\s+","").equals("scanbyid")){
                    i=new Intent(mActivity,OS_Search.class);
                    i.putExtra("scan","scan");
                }else{
                   i =new Intent(mActivity, AcitivityQuiz.class);
                    if(position==8){
                        i.putExtra("orderId",3);
                    }else if(position==1){
                        i.putExtra("orderId",4);
                    }else if(position==2){
                        i.putExtra("orderId",7);
                    }else if(position ==3){
                        i.putExtra("orderId",10);
                    }else if(position ==4){
                        i.putExtra("orderId",11);
                    }else if(position ==5){
                        i.putExtra("orderId",100);
                    }else if(position ==0){
                        i.putExtra("orderId",6);
                    }else if(position ==7){
                        i.putExtra("orderId",5);
                    }
                }

                startActivity(i);

            }
        });
    }
}
