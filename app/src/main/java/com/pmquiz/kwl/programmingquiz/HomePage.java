package com.pmquiz.kwl.programmingquiz;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import es.dmoral.toasty.Toasty;

public class HomePage extends AppCompatActivity {

    Button beginner, intermediate, advanced, score;

    private TextView title;
    private Toolbar mToolbar;
    final FragmentManager level_fragment= getSupportFragmentManager();
    Fragment fg1= new LevelFragment();
    Fragment fg3= new CallHistory();
    Fragment fg2= new SecondLevelFragment();

    Fragment fg4= new LevelFragment();
    Fragment active = fg1;
    private SharedPreferences mSharedPreferences;
    public static final String myShared= "javaMCQ";
    public static final String myShared_userName = "userName";
    Net_Con nc=new Net_Con();
    Bundle b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        mSharedPreferences = getSharedPreferences(myShared, Context.MODE_PRIVATE);

        ActionBar bar = getSupportActionBar();
        b=getIntent().getExtras();

        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#DD2D11")));

        bar.setTitle(mSharedPreferences.getString("userName",""));

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.bottom_navigation);
        fg3= new CallHistory();
        level_fragment.beginTransaction().add(R.id.fragment_container,fg4,"4").hide(fg4).commit();
        level_fragment.beginTransaction().add(R.id.fragment_container,fg3,"3").hide(fg3).commit();
        level_fragment.beginTransaction().add(R.id.fragment_container,fg2,"2").hide(fg2).commit();
        level_fragment.beginTransaction().add(R.id.fragment_container,active,"1").commit();
        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        Log.e("selected id",item.getItemId()+"");
                        if(nc.isNetworkConnected(HomePage.this)==true ){

                        }else{
                            Toasty.error(HomePage.this,"Please Turn on Internet",Toasty.LENGTH_LONG).show();
                        }
                        switch (item.getItemId()) {

                            case R.id.action_home:
                                level_fragment.beginTransaction().replace(R.id.fragment_container,fg1,"1").hide(fg1).commit();
                                level_fragment.beginTransaction().hide(active).show(fg1).commit();
                                active = fg1;
                                break;
                            case R.id.action_gg:
                                level_fragment.beginTransaction().replace(R.id.fragment_container,fg2,"2").hide(fg2).commit();
                                level_fragment.beginTransaction().hide(active).show(fg2).commit();
                                active = fg2;
                                break;
                            case R.id.action_message:
                                Toasty.error(HomePage.this,"Still developing",Toasty.LENGTH_LONG).show();
                                break;
                            case R.id.action_caller:
                                fg3  = new CallHistory();
                                level_fragment.beginTransaction().replace(R.id.fragment_container,fg3,"3").hide(fg3).commit();

                                level_fragment.beginTransaction().hide(active).show(fg3).commit();
                                active = fg3;
                                break;
                        }

                        return true;
                    }
                });

        if(b!=null){
            Log.e("HelloHome",b.getString("osOrderId")+"");
            if(b.getString("osOrderId") != null){
                SharedPreferences.Editor editor = mSharedPreferences.edit();
                editor.putString("oId","");
                editor.putString("oId",b.getString("osOrderId"));
                editor.apply();
                BottomSheetFragment bottomSheetFragment = new BottomSheetFragment();
                bottomSheetFragment.setCancelable(false);
                bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());



            }
            bottomNavigationView.setSelectedItemId(R.id.action_gg);

        }
        //Manually displaying the first fragment - one time only

    }
    @Override
    public void onBackPressed () {
        this.finishAffinity();
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mIf=getMenuInflater();
        mIf.inflate(R.menu.logout_menu,menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.logout:
                Log.e("hi there"," log out");
                SharedPreferences.Editor editor = mSharedPreferences.edit();
                editor.putString("name","");
                editor.putString("userName","");
                editor.putString("token","");
                editor.apply();
                Intent i = new Intent(HomePage.this,MainActivity.class);
                startActivity(i);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}