package com.pmquiz.kwl.programmingquiz;


import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.zxing.Result;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;


import java.io.File;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class AcitivityQuiz extends  AppCompatActivity implements ZXingScannerView.ResultHandler {
        private ZXingScannerView mScannerView;
        private SharedPreferences mSharedPreferences;
        public static final String myShared= "javaMCQ";
        public static final String myShared_userName = "name";
        Integer order_id;
        String o_id;

@Override
public void onCreate(Bundle state) {
        getSupportActionBar().hide();
        super.onCreate(state);
        mScannerView = new ZXingScannerView(this);
        setContentView(mScannerView);
        mSharedPreferences = getSharedPreferences(myShared, Context.MODE_PRIVATE);
        Bundle level= getIntent().getExtras();
        order_id=level.getInt("orderId");
        }

@Override
public void onResume() {
        super.onResume();
        // Register ourselves as a handler for scan results.
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
}

        @Override
        protected void onStop() {
                super.onStop();
                mScannerView.stopCamera();
        }


        @Override
        public void onBackPressed() {
                super.onBackPressed();
                mScannerView.stopCamera();
        }
@Override
public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
        }

@Override
public void handleResult(Result rawResult) {
        Log.e("resultqr", rawResult.getText());
        Log.e("result", rawResult.getBarcodeFormat().toString());
        Net_Con nc=new Net_Con();
        MediaPlayer mp =MediaPlayer.create(this,R.raw.beep);
        mp.start();
        //mp.stop();


        if(nc.isNetworkConnected(AcitivityQuiz.this)==true){
                sendItemStatus(rawResult.getText());
        }else{
                Toasty.error(AcitivityQuiz.this,"Please Turn on Internet",Toasty.LENGTH_LONG).show();
        }

        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
        }
public void sendItemStatus(String qrcode){
        AsyncHttpClient client = new AsyncHttpClient();
        JSONObject jsonParams = new JSONObject();
        try{
                jsonParams.put("qr_code",qrcode);
                Log.e("token ",mSharedPreferences.getString("token",""));
                client.addHeader("Authorization", "Bearer "+mSharedPreferences.getString("token",""));
                HttpEntity entity = new StringEntity(jsonParams.toString(),"UTF-8");
                client.post(getApplicationContext(),"http://mobile.dailyexpress.com.mm/api/v1/req_get_id_by_qr_code",entity,"application/json",new JsonHttpResponseHandler(){
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                                try {

                                        Integer status=new JSONObject(response.toString()).getInt("status");
                                        if(status==200){
                                                JSONObject serverResp = response.getJSONObject("data");
                                                o_id=serverResp.getString("order_id");
                                                if(order_id.equals(100)){
                                                        createAssignment(serverResp.getString("order_id"));
                                                }else if(order_id.equals(10) || order_id.equals(11)){
                                                        sendItem(serverResp.getString("order_id"),order_id);
                                                        Intent i=new Intent(AcitivityQuiz.this,RemarkActivity.class);
                                                        i.putExtra("msgId",o_id);
                                                        startActivity(i);
                                                } else{
                                                        sendItem(serverResp.getString("order_id"),order_id);
                                                }

                                        }else{
                                                Toast.makeText(AcitivityQuiz.this, "Token expired Please Login Again ", Toast.LENGTH_LONG).show();
                                                SharedPreferences.Editor editor = mSharedPreferences.edit();
                                                editor.putString("name","");
                                                editor.putString("userName","");
                                                editor.putString("token","");
                                                editor.apply();
                                                Intent i = new Intent(AcitivityQuiz.this,MainActivity.class);
                                                startActivity(i);
                                                finish();

                                        }
                                } catch (JSONException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                }
                        }

                        @Override
                        public void onFailure(int statusCode,Header[] headers,String response,Throwable tb){

                                Toast.makeText(AcitivityQuiz.this, " Failure "+statusCode,
                                        Toast.LENGTH_LONG).show();
                        }

                });
        }catch (JSONException e){
                Toast.makeText(AcitivityQuiz.this, " Failure "+e.getMessage(),
                        Toast.LENGTH_LONG).show();
        }


}


public void sendItem(final String orderId, Integer val){
        String userId="" ;if(mSharedPreferences.contains("name")){
               userId =mSharedPreferences.getString("name","");
        }

        AsyncHttpClient client = new AsyncHttpClient();
        JSONObject jsonParams = new JSONObject();
        try{
                jsonParams.put("user",userId);jsonParams.put("pk",orderId);jsonParams.put("value",val.toString());
                HttpEntity entity = new StringEntity(jsonParams.toString(),"UTF-8");
                client.addHeader("Authorization", "Bearer "+mSharedPreferences.getString("token",""));
                client.post(getApplicationContext(),"http://mobile.dailyexpress.com.mm/api/v1/req_change_stat",entity,"application/json",new JsonHttpResponseHandler(){
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                                try {
                                        JSONObject serverResp = new JSONObject(response.toString());
                                        if(serverResp.getInt("status")==200){
                                                Toasty.success(AcitivityQuiz.this, serverResp.getString("msg"),
                                                        Toast.LENGTH_LONG,true).show();
                                                if(order_id.equals(7)){
                                                        Intent i = new Intent(AcitivityQuiz.this,SignActivity.class);
                                                        i.putExtra("idP",o_id);
                                                        startActivity(i);
                                                        finish();
                                                }
                                        }else{

                                                Toast.makeText(AcitivityQuiz.this, "Token expired Please Login Again ", Toast.LENGTH_LONG).show();
                                                SharedPreferences.Editor editor = mSharedPreferences.edit();
                                                editor.putString("name","");
                                                editor.putString("userName","");
                                                editor.putString("token","");
                                                editor.apply();
                                                Intent i = new Intent(AcitivityQuiz.this,MainActivity.class);
                                                startActivity(i);
                                                finish();
                                        }

                                } catch (JSONException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                }
                        }

                        @Override
                        public void onFailure(int statusCode,Header[] headers,String response,Throwable tb){
                                ;
                                Toasty.error(AcitivityQuiz.this, response,
                                        Toast.LENGTH_LONG,true).show();
                        }

                });
        }catch (JSONException e){

        }

}


        public void createAssignment(final String orderId){
                AsyncHttpClient client = new AsyncHttpClient();
                JSONObject jsonParams = new JSONObject();
                String userId="" ;if(mSharedPreferences.contains("name")){
                        userId =mSharedPreferences.getString("name","");
                }
                try {
                        jsonParams.put("user",userId);jsonParams.put("order_id",orderId);
                        HttpEntity entity = new StringEntity(jsonParams.toString(),"UTF-8");
                        client.addHeader("Authorization", "Bearer "+mSharedPreferences.getString("token",""));
                        client.post(getApplicationContext(),"http://mobile.dailyexpress.com.mm/api/v1/req_create_assignment",entity,"application/json",new JsonHttpResponseHandler(){
                                @Override
                                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                                        try {
                                                JSONObject serverResp = new JSONObject(response.toString());
                                                if(serverResp.getInt("status")==200){
                                                        Toasty.success(AcitivityQuiz.this, serverResp.getString("msg"),
                                                                Toast.LENGTH_LONG,true).show();

                                                        Intent i = new Intent(AcitivityQuiz.this,Assign.class);
                                                        i.putExtra("itemState","Assign");
                                                        startActivity(i);
                                                        finish();
                                                }else{
                                                        Toast.makeText(AcitivityQuiz.this, "Token expired Please Login Again ", Toast.LENGTH_LONG).show();
                                                        SharedPreferences.Editor editor = mSharedPreferences.edit();
                                                        editor.putString("name","");
                                                        editor.putString("userName","");
                                                        editor.putString("token","");
                                                        editor.apply();
                                                        Intent i = new Intent(AcitivityQuiz.this,MainActivity.class);
                                                        startActivity(i);
                                                        finish();
                                                }


                                        } catch (JSONException e) {
                                                // TODO Auto-generated catch block
                                                e.printStackTrace();
                                        }
                                }

                                @Override
                                public void onFailure(int statusCode,Header[] headers,String response,Throwable tb){
                                        ;
                                        Toasty.error(AcitivityQuiz.this, response,
                                                Toast.LENGTH_LONG,true).show();
                                }

                        });
                }catch (JSONException e){

                }

        }
}


