package com.pmquiz.kwl.programmingquiz;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.Toast;

import com.google.gson.JsonIOException;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import es.dmoral.toasty.Toasty;

public class OSResultFragment extends Fragment  {

    private final static String MIPMAP = "mipmap";

    private RecyclerView mRecycleView;
    private Activity mActivity;
    private CardView layout_card;
    JSONArray resultArray;
    Bundle b;
    String idP;
    String conditionId;
    private SharedPreferences mSharedPreferences;
    public static final String myShared= "javaMCQ";
    public static final String myShared_userName = "name";

    public OSResultFragment() {

    }

    public static LevelFragment newInstance() {
        LevelFragment fragment = new LevelFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        b=getActivity().getIntent().getExtras();
        mSharedPreferences = getActivity().getSharedPreferences(myShared, Context.MODE_PRIVATE);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Intent i = getActivity().getIntent();
        String json =i.getSerializableExtra("osResult")+"";
        Log.e("scan os result ",json);

        try{
            if(b.getString("scanId").isEmpty()|| b.getString("scanId").equals("c2m")){
                resultArray= new JSONArray(json);
            }else{

                JSONObject aa = new JSONObject(i.getSerializableExtra("osResult")+"");
                aa.put("data", json);
                Log.e("hello",aa+"");
                resultArray = new JSONArray();
                resultArray.put(aa);
            }


        }catch (JSONException e){
            Log.e("errjson",e+"");
        }


        return inflater.inflate(R.layout.os_search_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mActivity = getActivity();
        mRecycleView = (RecyclerView) view.findViewById(R.id.osResult_fragment);
        try{

            setLevelView();
            Log.e("lakwan",resultArray.length()+"");
        }catch (JSONException e){

        }
    }

    private void setLevelView()throws JSONException  {


        List<OS_result> resultlist= loadData(resultArray);

        OSListView levelAdapterView = new OSListView(resultlist, getContext());
        setOnclickListener(levelAdapterView, resultlist);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mActivity, 1);
       mRecycleView.setLayoutManager(gridLayoutManager);
       mRecycleView.setAdapter(levelAdapterView);
    }

    private List<OS_result> loadData(JSONArray resultedArray) {


       List<OS_result> dataList=new ArrayList<OS_result>();

        try{
            if(b.getString("scanId").isEmpty()||b.getString("scanId").equals("c2m")){
                for(int j=0;j<resultedArray.length();j++) {
                    JSONObject iconName = resultedArray.getJSONObject(j);
                    OS_result aa = new OS_result();
                    Log.e("osResult",iconName.toString());
                    aa.setUserName(iconName.getString("name"));
                    aa.setPhoneNo(iconName.getString("phone"));
                    aa.setUseraddress(iconName.getString("address"));
                    aa.setItem_id(iconName.getString("id"));
                    if(!b.getString("scanId").isEmpty() && b.getString("scanId").equals("c2m")){
                        aa.setCheck(iconName.getString("township_id"));
                    }else{

                        aa.setCheck(iconName.getString("city_id"));
                    }

                    dataList.add(aa);
                    //int icon = getResourceByName(iconName,"drawable", resources, packageName);
                    //level.setIcon_id(icon);
                }
            }else {

                for(int j=0;j<resultedArray.length();j++) {
                    JSONObject iconName = resultedArray.getJSONObject(j);
                    OS_result aa = new OS_result();

                    aa.setUserName(iconName.getString("name"));
                    aa.setPhoneNo(iconName.getString("phone"));
                    aa.setUseraddress(iconName.getString("address"));
                    aa.setItem_id(iconName.getString("order_id"));

                    if(iconName.getString("qr_code")!=null){
                        aa.setTrackingCode("Tracking Code #"+iconName.getString("qr_code"));
                    }else{
                        aa.setTrackingCode("");
                    }

                    if(iconName.getString("status")!=null){
                        aa.setItemStatus(iconName.getString("status"));
                    }else{
                        aa.setItemStatus("");
                    }
                    dataList.add(aa);
                    //int icon = getResourceByName(iconName,"drawable", resources, packageName);
                    //level.setIcon_id(icon);
                }
            }

        }catch (JSONException e){

        }


        return dataList;
    }

    private int getResourceByName(String name, String identifier, Resources resources, String packageName) {
        return resources.getIdentifier(name, identifier, packageName);
    }

    private void setOnclickListener(final OSListView levelAdapterView, final List<OS_result> osResultList) {
        levelAdapterView.setOnItemClickListener(new OSListView.OnItemClickListener() {

            @Override
            public void onClick(View view, final int position) {
                if(b.getString("scanId").isEmpty()){
                    Intent i =new Intent(mActivity, CreateOrder.class);
                    //Pending 0, Collected 3, Transferring 6, Received 5, Delivering 4,Delivered 7, Error 10, Cancel 11
                    i.putExtra("os",osResultList.get(position).getUserName());
                    i.putExtra("osPh",osResultList.get(position).getPhoneNo());
                    i.putExtra("osAdd",osResultList.get(position).getUseraddress());
                    i.putExtra("osID",osResultList.get(position).getItem_id());
                    i.putExtra("townId",osResultList.get(position).getCheck());
                    Log.e("going",osResultList.get(position).getCheck());
                    startActivity(i);
                    mActivity.finish();
                    Log.e("DATA ",osResultList.get(position).getUserName());
                }else  if(b.getString("scanId").equals("c2m")){
                    Intent i =new Intent(mActivity, C2MOrder.class);

                    //Pending 0, Collected 3, Transferring 6, Received 5, Delivering 4,Delivered 7, Error 10, Cancel 11
                    i.putExtra("os",osResultList.get(position).getUserName());
                    i.putExtra("osPh",osResultList.get(position).getPhoneNo());
                    i.putExtra("osAdd",osResultList.get(position).getUseraddress());
                    i.putExtra("osID",osResultList.get(position).getItem_id());
                    i.putExtra("townId",osResultList.get(position).getCheck());
                    startActivity(i);
                    mActivity.finish();
                    Log.e("DATA ",osResultList.get(position).getUserName());
                }else{
                    Intent i = new Intent(mActivity,HomePage.class);
                    String osId = osResultList.get(position).getItem_id();
                   i.putExtra("osOrderId",osId);
                   startActivity(i);
                   mActivity.finish();
                }
//                    Context wrapper = new ContextThemeWrapper(getActivity().getApplicationContext(), R.style.MyPopupOtherStyle);
//                    PopupMenu popupMenu=new PopupMenu(wrapper,view,Gravity.BOTTOM);
//
//
//                    /*  The below code in try catch is responsible to display icons*/
//                    try {
//                        Field[] fields = popupMenu.getClass().getDeclaredFields();
//                        for (Field field : fields) {
//                            if ("mPopup".equals(field.getName())) {
//                                field.setAccessible(true);
//                                Object menuPopupHelper = field.get(popupMenu);
//                                Class<?> classPopupHelper = Class.forName(menuPopupHelper.getClass().getName());
//                                Method setForceIcons = classPopupHelper.getMethod("setForceShowIcon", boolean.class);
//                                setForceIcons.invoke(menuPopupHelper, true);
//                                break;
//                            }
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//
//                    popupMenu.getMenuInflater()
//                            .inflate(R.menu.popup_menu, popupMenu.getMenu());
//                    popupMenu.show();
//                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
//                        public boolean onMenuItemClick(MenuItem item) {
//                            //Pending 0, Collected 3, Transferring 6, Received 5, Delivering 4,Delivered 7, Error 10, Cancel 11
//                            Log.e("Order",osResultList.get(position).getItem_id()+"");
//                          if(item.getTitle().equals("Collected")){
//                              sendItem(osResultList.get(position).getItem_id(),3);
//                          }else if(item.getTitle().equals("Transferring")){
//                              sendItem(osResultList.get(position).getItem_id(),6);
//                          }else if(item.getTitle().equals("Received")){
//                              sendItem(osResultList.get(position).getItem_id(),5);
//                          }else if(item.getTitle().equals("Delivering")){
//                              sendItem(osResultList.get(position).getItem_id(),4);
//                          }else if(item.getTitle().equals("Delivered")){
//                              sendItem(osResultList.get(position).getItem_id(),7);
//                          }else if(item.getTitle().equals("Error")){
//                              sendItem(osResultList.get(position).getItem_id(),10);
//                          }else if(item.getTitle().equals("Cancel")){
//                              sendItem(osResultList.get(position).getItem_id(),11);
//                          }
//                            return true;
//                        }
//                    });
//
//
//                }


            }
        });
    }


    public void sendItem(final String orderId, Integer val){
        idP=orderId;
        conditionId= val.toString();
        RequestParams rp = new RequestParams();
        String userId="" ;if(mSharedPreferences.contains("name")){
            userId =mSharedPreferences.getString("name","");
        }
        rp.add("user",userId);rp.add("pk",orderId);rp.add("value",val.toString());
        Log.e("request ",rp.toString());

        HttpUtils.get("req_change_stat.php?",rp,new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    JSONObject serverResp = new JSONObject(response.toString());
                    if(serverResp.getString("err").equals("0")){
                        Toasty.success(getActivity().getApplicationContext(),"Successfully Updated",Toasty.LENGTH_LONG).show();
                       if(conditionId.equals("7")){
                            Intent i=new Intent(getActivity().getApplicationContext(),SignActivity.class);
                            i.putExtra("idP",idP);
                            startActivity(i);
                            mActivity.finish();
                       }else if(conditionId.equals("10")||conditionId.equals("11")){
                           Intent i=new Intent(getActivity().getApplicationContext(),RemarkActivity.class);
                           i.putExtra("msgId",idP);
                           startActivity(i);
                           mActivity.finish();
                       }else{
                           Intent intent = new Intent(getActivity().getApplicationContext(), HomePage.class);
                           startActivity(intent);
                           mActivity.finish();
                       }
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode,Header[] headers,String response,Throwable tb){
                ;
                Toasty.error(getActivity().getApplicationContext(), response,
                        Toast.LENGTH_LONG,true).show();
            }

        });
    }
}
