package com.pmquiz.kwl.programmingquiz;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;


import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import net.steamcrafted.loadtoast.LoadToast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.entity.StringEntity;

public class AssignFragment extends Fragment  {



    private RecyclerView mRecycleView;
    private Activity mActivity;
    private CardView layout_card;
    JSONArray resultArray;
    LoadToast loadToast;
    Handler handler = new Handler();
    Boolean isLoading = false;
    ProgressBar pg;
    String state= "";

    private SharedPreferences sharedPreferences;

    public AssignFragment() {

    }

    public static AssignFragment newInstance() {
        AssignFragment fragment = new AssignFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Intent i = getActivity().getIntent();
        String json =i.getSerializableExtra("assignList")+"";
        state = i.getStringExtra("clickedState");
        try{
            resultArray= new JSONArray(json);
        }catch (JSONException e){
            Log.e("errjson",e+"");
        }


        return inflater.inflate(R.layout.assign_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.e("onview created",savedInstanceState+"");
        mActivity = getActivity();
        sharedPreferences = mActivity.getSharedPreferences("javaMCQ",Context.MODE_PRIVATE);

        mRecycleView = (RecyclerView) view.findViewById(R.id.assign_fragment);

        mRecycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {


                super.onScrollStateChanged(recyclerView, newState);
                GridLayoutManager gridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();


                if (gridLayoutManager != null && gridLayoutManager.findLastCompletelyVisibleItemPosition() == resultArray.length() - 1 && newState==0) {
                    //bottom of list!

//                    RelativeLayout layout = new RelativeLayout(getActivity());
//                    ProgressBar progressBar = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleSmall);
//                    progressBar.setIndeterminate(true);
//                    progressBar.setVisibility(View.VISIBLE);
//                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100,100);
//                    params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//                    layout.addView(progressBar,params);
//
//                    getActivity().setContentView(layout);
//                    ProgressDialog proDialog = new ProgressDialog(getActivity());
//                    proDialog.getWindow().setBackgroundDrawable( new ColorDrawable( Color.TRANSPARENT ) );
//                    proDialog.getWindow().setGravity(Gravity.BOTTOM);
//                    proDialog.getWindow().setGravity(Gravity.CENTER);
//                    proDialog.show();

                 // Log.e("ggprogrss",progressBar.isShown()+"");
//                    int bottomOfScreen = getResources().getDisplayMetrics().heightPixels;
//                    loadToast =new LoadToast(getActivity()).setBorderColor(Color.TRANSPARENT).setBackgroundColor(Color.WHITE).setProgressColor(Color.BLUE)
//                            .setTranslationY(bottomOfScreen-250).show();
//
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//
//                            loadMore();
//                        }
//                    }, 5000);


                    pg=(ProgressBar) getActivity().findViewById(R.id.recycleLoad);
                    pg.setBackgroundColor(Color.TRANSPARENT);
                    pg.setVisibility(View.VISIBLE);
                    loadMore();





                }


            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {

                super.onScrolled(recyclerView, dx, dy);



            }
        });


        try{
            setLevelView();
        }catch (JSONException e){

        }
    }




    private void setLevelView()throws JSONException  {


        List<OS_result> resultlist= loadData(resultArray);

        Log.e("chck", resultlist+"");
        AssignAdapter levelAdapterView; levelAdapterView = new AssignAdapter(resultlist, getContext(),state);

        setOnclickListener(levelAdapterView, resultlist);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mActivity, 1);
//        if(isLoading==true){
//            loadToast.success();
//            isLoading=false;
//            loadToast.hide();
//        }
        mRecycleView.setLayoutManager(gridLayoutManager);
        mRecycleView.setAdapter(levelAdapterView);


    }

    private List<OS_result> loadData(JSONArray resultArray) throws JSONException {
        Resources resources = mActivity.getResources();
        String packageName = mActivity.getPackageName();

        List<OS_result> result=new ArrayList<>();
        for(int j=0;j<resultArray.length();j++) {
            JSONObject iconName = resultArray.getJSONObject(j);
            Log.e("iconNae",iconName.getString("name"));
            OS_result aa = new OS_result();
            aa.setUserName(iconName.getString("name"));
            aa.setPhoneNo(iconName.getString("phone"));
            aa.setUseraddress(iconName.getString("address"));

            aa.setFormatted_date(iconName.getString("formatted_date"));
            aa.setTrackingCode(iconName.getString("qr_code"));
            aa.setS_Name(iconName.getString("sender"));
            aa.setS_Phone(iconName.getString("sender_phone"));

            aa.setItemCod(iconName.getString("cod"));
            aa.setItemStatus(iconName.getString("status"));
            aa.setItemAmount(iconName.getString("price"));
            aa.setDcType(iconName.getString("dc_type"));
            aa.setItemName(iconName.getString("item_name"));

            aa.setRemark_item(iconName.getString("remark"));

            result.add(aa);
            //int icon = getResourceByName(iconName,"drawable", resources, packageName);
            //level.setIcon_id(icon);
        }

        return result;
    }

    private int getResourceByName(String name, String identifier, Resources resources, String packageName) {
        return resources.getIdentifier(name, identifier, packageName);
    }

    private void setOnclickListener(final AssignAdapter levelAdapterView, final List<OS_result> osResultList) {
        levelAdapterView.setOnItemClickListener(new AssignAdapter.OnItemClickListener() {

            @Override
            public void onClick(View view, int position) {
                Intent i =new Intent(mActivity, OrderDetail.class);

                OS_result gg =osResultList.get(position);
                Log.e("oooo",position+"");
                i.putExtra("order","");
                i.putExtra("order",gg);
          i.putExtra("osAdd",osResultList.get(position).getUseraddress());
                startActivity(i);
            }
        });
    }

   public void loadMore(){
        Intent i =getActivity().getIntent();
       List<OS_result> resultlist =new ArrayList<>();

       AsyncHttpClient client = new AsyncHttpClient();
       JSONObject jsonParams=new JSONObject();
       try {
           jsonParams = new JSONObject(i.getStringExtra("rp"));
           jsonParams.remove("last_call");
           jsonParams.put("last_call",resultArray.getJSONObject(resultArray.length()-1).getString("order_id"));


       }catch (JSONException e){

       }

       HttpEntity entity =  new StringEntity(jsonParams.toString(),"UTF-8");
       client.addHeader("Authorization","Bearer "+sharedPreferences.getString("token",""));

       client.post(mActivity.getApplicationContext(),"http://mobile.dailyexpress.com.mm/api/v1/req_orderlist", entity,"application/json", new JsonHttpResponseHandler() {

           @Override
           public void onSuccess(int statusCode, Header[] headers, JSONObject response) {


               try {
                   JSONObject serverResp = new JSONObject(response.toString());
                   if (serverResp.getInt("status")==200) {
                       pg=(ProgressBar) getActivity().findViewById(R.id.recycleLoad);
                       pg.setVisibility(View.GONE);
                      JSONArray newJson = serverResp.getJSONArray("data");
                       Integer positon = resultArray.length();
                       if(newJson.length()>0 && !resultArray.getJSONObject(resultArray.length()-1).getString("order_id").equals(newJson.getJSONObject(0).getString("order_id"))){

                           for (int i = 0; i < newJson.length(); i++) {
                               JSONObject jsonObject = newJson.getJSONObject(i);
                               resultArray.put(jsonObject);
                           }
//                       mDialog.setContentView(null);

                           setLevelView();
//                           loadToast.success();
//
//                           loadToast.hide();

                          // progressBar.setVisibility(View.INVISIBLE);
                       }else{
                           Log.e("else "," no more data");
//                           loadToast.setText("No More Data");
//                           loadToast.hide();

                       }

                       mRecycleView.getLayoutManager().scrollToPosition(positon);


                   } else {
                       pg=(ProgressBar) getActivity().findViewById(R.id.recycleLoad);
                       pg.setVisibility(View.GONE);
                       Toast.makeText(mActivity.getApplicationContext(), "Token expired Please Login Again ", Toast.LENGTH_LONG).show();
                       SharedPreferences.Editor editor = sharedPreferences.edit();
                       editor.putString("name","");
                       editor.putString("userName","");
                       editor.putString("token","");
                       editor.apply();
                       Intent i = new Intent(mActivity.getApplicationContext(),MainActivity.class);
                       startActivity(i);
                        mActivity.finish();
                   }
               } catch (JSONException e) {
                   // TODO Auto-generated catch block
                   e.printStackTrace();
               }
           }

           @Override
           public void onFailure(int statusCode, Header[] headers, String response, Throwable tb) {

           }

       });
   }


}
