package com.pmquiz.kwl.programmingquiz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;


public class C2MOrder extends AppCompatActivity {

    private SharedPreferences mSharedPreferences;
    EditText receiverName,receiverPh,receiverAdd,cod_fee,remark,item_length,item_width,item_weight,item_height,item_description,item_code,t_code;
    Spinner receiver_city;
    RadioGroup payment;
    Button submit;
    List<City> city_list=new ArrayList<>();
    String[] spinnerArray;
    HashMap<Integer,String> spinnerMap ;
    ProgressBar pg;
    String id_sender="0";
    String townId ="";
    AsyncHttpClient client = new AsyncHttpClient();
    public void onCreate(Bundle savedInstanceState) {
        Typeface zawgyi= Typeface.createFromAsset(getAssets(),"Unicode.ttf");

        super.onCreate(savedInstanceState);
        mSharedPreferences = getSharedPreferences("javaMCQ", Context.MODE_PRIVATE);

        setContentView(R.layout.c2m_order);
        submit =(Button) findViewById(R.id.c2msubmit);
        Net_Con nc =new Net_Con();
        if(nc.isNetworkConnected(C2MOrder.this)==true ){

        }else{
            Toasty.error(C2MOrder.this,"Please Turn on Internet",Toasty.LENGTH_LONG).show();
        }

        pg=(ProgressBar) findViewById(R.id.orderProgress);


        ActionBar bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#DD2D11")));
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.main_action_bar);
        TextView tt=(TextView) findViewById(R.id.action_bar_title);
        tt.setText("C2M Order");

        Bundle level= getIntent().getExtras();


        ScrollView mainScrollView =(ScrollView) findViewById(R.id.createScroll);
        mainScrollView.fullScroll(ScrollView.FOCUS_UP);


        receiver_city=(Spinner) findViewById(R.id.receiver_city) ;
        getCity();







        //receiver section
        TextInputLayout rcname = (TextInputLayout)findViewById(R.id.receiver_layout_name);
        rcname.setTypeface(zawgyi);
        rcname.setHint("အမည်");
        TextInputLayout rcphNo = (TextInputLayout)findViewById(R.id.receiver_layout_phone);
        rcphNo.setTypeface(zawgyi);
        rcphNo.setHint("ဖုန်းနံပါတ်");
        TextInputLayout rcaddress = (TextInputLayout)findViewById(R.id.receiver_layout_address);
        rcaddress.setTypeface(zawgyi);
        rcaddress.setHint("လိပ်စာ");
        TextView cityName= (TextView) findViewById(R.id.c2m_CityName);
        cityName.setTypeface(zawgyi);
        cityName.setText("မြို့");
        TextInputLayout idCode =(TextInputLayout) findViewById(R.id.item_code_layout);
        idCode.setTypeface(zawgyi);
        idCode.setHint("Tracking");




        receiverName=(EditText) findViewById(R.id.c2m_receiver);
        receiverPh =(EditText) findViewById(R.id.c2m_receiver_ph);
        receiverAdd=(EditText) findViewById(R.id.c2m_receiver_address);
        if(level!=null){
            receiverName.setText(level.getString("os"));
            disableEditText(receiverName);
            receiverPh.setText(level.getString("osPh"));
            disableEditText(receiverPh);
            receiverAdd.setText(level.getString("osAdd"));
            disableEditText(receiverAdd);
            id_sender=level.getString("osID");//city_id township_id
            townId = level.getString("townId");

        }
        receiverName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()!=0){
                    checkEmpty();
                }else {
                    receiverName.setError("Please Enter receiver name");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        receiverPh.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()!=0){
                    checkEmpty();
                }else {
                    receiverPh.setError("Please Enter receiver phone number");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        receiverAdd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()!=0){
                    checkEmpty();
                }else {
                    receiverAdd.setError("Please Enter receiver address");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        //item section
        TextInputLayout itemLength = (TextInputLayout)findViewById(R.id.item_length_layout);
        itemLength.setTypeface(zawgyi);
        itemLength.setHint("အလျား");
        TextInputLayout itemWidth = (TextInputLayout)findViewById(R.id.item_width_layout);
        itemWidth.setTypeface(zawgyi);
        itemWidth.setHint("အနံ");
        TextInputLayout itemheight = (TextInputLayout)findViewById(R.id.item_height_layout);
        itemheight.setTypeface(zawgyi);
        itemheight.setHint("အမြင့်");
        TextInputLayout itemweight= (TextInputLayout) findViewById(R.id.item_weight_layout);
        itemweight.setTypeface(zawgyi);
        itemweight.setHint("အလေးချိန်");
        TextInputLayout itemdes= (TextInputLayout) findViewById(R.id.item_desc_layout);
        itemdes.setTypeface(zawgyi);
        itemdes.setHint("ပစ္စည်းအမျိုးအစား");
        TextInputLayout itemrm= (TextInputLayout) findViewById(R.id.item_rm_layout);
        itemrm.setTypeface(zawgyi);
        itemrm.setHint("မှတ်ချက်");
        TextInputLayout cod= (TextInputLayout) findViewById(R.id.item_cod_layout);
        cod.setTypeface(zawgyi);
        cod.setHint("ငွေကောက်ရန်");

        item_length=(EditText) findViewById(R.id.item_length);
        item_width =(EditText) findViewById(R.id.item_width);
        item_height=(EditText) findViewById(R.id.item_height);
        item_weight=(EditText) findViewById(R.id.item_weight);
        item_description=(EditText) findViewById(R.id.item_desc);
        remark =(EditText) findViewById(R.id.item_rm);
        cod_fee=(EditText) findViewById(R.id.item_cod);
        payment = (RadioGroup) findViewById(R.id.payment);
        item_code=(EditText) findViewById(R.id.item_code);

        item_code.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()!=0){
                    checkEmpty();
                }else {
                    item_code.setError("Please Enter item length");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        item_length.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()!=0){
                    checkEmpty();
                }else {
                    item_length.setError("Please Enter item length");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        item_width.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()!=0){
                    checkEmpty();
                }else {
                    item_width.setError("Please Enter item width");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        item_height.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()!=0){
                    checkEmpty();
                }else {
                    item_height.setError("Please Enter item height");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        item_weight.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()!=0){
                    checkEmpty();
                }else {
                    item_weight.setError("Please Enter item weight");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        item_description.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()!=0){
                    checkEmpty();
                }else {
                    item_description.setError("Please Enter item description");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        cod_fee.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()!=0){
                    if(item_code.getText().toString().isEmpty()||receiverName.getText().toString().isEmpty()||receiverPh.getText().toString().isEmpty()||receiverAdd.getText().toString().isEmpty()||item_length.getText().toString().isEmpty()||item_weight.getText().toString().isEmpty()||item_width.getText().toString().isEmpty()||item_description.getText().toString().isEmpty()||item_height.getText().toString().isEmpty()||cod_fee.getText().toString().isEmpty()){
                        Toasty.error(C2MOrder.this,"Please Fill For Order",Toasty.LENGTH_LONG).show();
                    }else{
                        submit.setAlpha(1);
                        submit.setEnabled(true);
                        submit.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View view) {
                                pg.setVisibility(View.VISIBLE);
                                Net_Con nc =new Net_Con();
                                if(nc.isNetworkConnected(C2MOrder.this)==true ){
                                    submit.setAlpha((float)0.5);
                                    submit.setEnabled(false);
                                    postData();
                                }else{
                                    Toasty.error(C2MOrder.this,"Please Turn on Internet",Toasty.LENGTH_LONG).show();
                                }

                            }
                        });
                    }
                }else{
                    cod_fee.setError("Please Enter COD FEE");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                pg.setVisibility(View.VISIBLE);
                Net_Con nc =new Net_Con();
                if(nc.isNetworkConnected(C2MOrder.this)==true ){
                    submit.setAlpha((float)0.5);
                    submit.setEnabled(false);
                    postData();
                }else{
                    Toasty.error(C2MOrder.this,"Please Turn on Internet",Toasty.LENGTH_LONG).show();
                }

            }
        });


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void getCity(){
        AsyncHttpClient client = new AsyncHttpClient();

        client.addHeader("Authorization", "Bearer "+mSharedPreferences.getString("token", ""));

        client.get(getApplicationContext(),"http://mobile.dailyexpress.com.mm/api/v1/get_township_list",new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArray
                try {
                    JSONObject serverResp = new JSONObject(response.toString());
                    if(serverResp.getInt("status")==200){
                        JSONArray jsonArray=serverResp.getJSONArray("data");
                        for(int j=0;j<jsonArray.length();j++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(j);
                            City city = new City(jsonObject1.getString("township_id"),jsonObject1.getString("name"));
                            Log.e("get city",city.getId()+" "+city.getName()+" "+townId);
                            city_list.add(city);
                        }
                        setDatatoSpinner(receiver_city);
                    }else{
                        Toast.makeText(C2MOrder.this, "Token expired Please Login Again ", Toast.LENGTH_LONG).show();
                        SharedPreferences.Editor editor = mSharedPreferences.edit();
                        editor.putString("name","");
                        editor.putString("userName","");
                        editor.putString("token","");
                        editor.apply();
                        Intent i = new Intent(C2MOrder.this,MainActivity.class);
                        startActivity(i);
                        finish();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode,Header[] headers,String response,Throwable tb){


            }

        });


    }

    public void setDatatoSpinner(Spinner receiverCity){
        spinnerArray = new String[city_list.size()];
        spinnerMap = new HashMap<Integer, String>();
        for (int i = 0; i < city_list.size(); i++)
        {
            spinnerMap.put(i,city_list.get(i).getId());
            spinnerArray[i] = city_list.get(i).getName();

            if(townId!="" && townId.equals(city_list.get(i).getId())){
                Log.e("checkTOwnid", townId+" "+city_list.get(i).getId());
                townId =i+"";
            }


        }


        ArrayAdapter<String> adapter =new ArrayAdapter<String>(getApplicationContext(),R.layout.spinner_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        if(!townId.isEmpty()&&townId!="") {
            receiver_city.setEnabled(false);
            receiver_city.setClickable(false);
        }
        receiver_city.setAdapter(adapter);
        if(!townId.isEmpty()&&townId!=""){
//           spinnerMap.get(townId);


            receiver_city.setSelection(Integer.parseInt(townId));
        }
    }
    public void checkEmpty(){
        if((receiverName.getText()!=null && receiverName.getText().toString().isEmpty())||(receiverPh.getText()!=null && receiverPh.getText().toString().isEmpty())||(receiverAdd.getText()!=null && receiverAdd.getText().toString().isEmpty())||(item_length.getText()!=null && item_length.getText().toString().isEmpty())||(item_weight.getText()!=null && item_weight.getText().toString().isEmpty())||(item_width.getText()!=null && item_width.getText().toString().isEmpty())||(item_description.getText()!=null && item_description.getText().toString().isEmpty())||(item_height.getText()!=null && item_height.getText().toString().isEmpty())||(cod_fee.getText()!=null && cod_fee.getText().toString().isEmpty())||(item_code.getText()!=null && item_code.getText().toString().isEmpty())){
            submit.setAlpha((float)0.5);
            submit.setEnabled(false);
        }else{
            submit.setAlpha(1);
            submit.setEnabled(true);
            submit.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    pg.setVisibility(View.VISIBLE);
                    Net_Con nc =new Net_Con();
                    if(nc.isNetworkConnected(C2MOrder.this)==true ){
                        submit.setAlpha((float)0.5);
                        submit.setEnabled(false);
                        postData();
                    }else{
                        Toasty.error(C2MOrder.this,"Please Turn on Internet",Toasty.LENGTH_LONG).show();
                    }

                }
            });
        }
    }
    private void disableEditText(EditText editText) {
        editText.setFocusable(false);
        editText.setEnabled(false);
        editText.setCursorVisible(false);
        editText.setKeyListener(null);
        editText.setBackgroundColor(Color.TRANSPARENT);
    }

    public void postData() {

        JSONObject jsonParams = new JSONObject();
        Log.e("idLay",id_sender+""+item_code.getText().toString());


        try {
            // Add your data
            Log.e("ggg",receiver_city.getSelectedItemPosition()+"");

            jsonParams.put("sender_id", id_sender);
            jsonParams.put("receiver_name", receiverName.getText().toString());
            jsonParams.put("receiver_phone", receiverPh.getText().toString());
            jsonParams.put("receiver_address", receiverAdd.getText().toString());
            jsonParams.put("destination_id", spinnerMap.get(receiver_city.getSelectedItemPosition()));

            jsonParams.put("length", item_length.getText().toString());
            jsonParams.put("width", item_width.getText().toString());
            jsonParams.put("height", item_height.getText().toString());
            jsonParams.put("remark",remark.getText().toString());
            jsonParams.put("qr_code",item_code.getText().toString());

            jsonParams.put("weight",item_weight.getText().toString());
            jsonParams.put("cod_fees",cod_fee.getText().toString());


//            jsonParams.put("tracking_code",t_code.getText().toString());
            jsonParams.put("item_desc",item_description.getText().toString());

            jsonParams.put("quantity","1");


            HttpEntity entity = new StringEntity(jsonParams.toString(),"UTF-8");
            client.addHeader("Authorization", "Bearer "+mSharedPreferences.getString("token", ""));
            client.post(getApplicationContext(), "http://mobile.dailyexpress.com.mm/api/v1/c2m_submit_order", entity, "application/json",
                    new JsonHttpResponseHandler(){

                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            // If the response is JSONObject instead of expected JSONArray
                            Log.e("order ", response.toString());
                            try {
                                JSONObject serverResp = new JSONObject(response.toString());

                                if(serverResp.getInt("status")==200){

                                    JSONObject json=serverResp.getJSONObject("data");
                                    OS_result aa = new OS_result();
                                    aa.setUserName(json.getString("name"));
                                    aa.setPhoneNo(json.getString("phone"));
                                    aa.setUseraddress(json.getString("address"));

                                    aa.setTrackingCode(json.getString("qr_code"));
                                    aa.setS_Name("C2M");
                                    aa.setS_Phone("*******");

                                    aa.setItemCod(json.getString("cod"));
                                    aa.setItemStatus(json.getString("status"));
                                    aa.setItemAmount(json.getString("price"));
                                    aa.setDcType(json.getString("dc_type"));
                                    aa.setItemName(json.getString("item_name"));

                                    aa.setRemark_item(json.getString("remark"));
                                    aa.setItem_id(json.getString("order_id"));

                                    Intent i = new Intent(C2MOrder.this,OrderDetail.class);
                                    i.putExtra("order","");
                                    i.putExtra("order",aa);
                                    i.putExtra("createOrder","CO");

                                    startActivity(i);
                                    finish();
                                }else{
                                    Toast.makeText(C2MOrder.this, "Token expired Please Login Again ", Toast.LENGTH_LONG).show();
                                    SharedPreferences.Editor editor = mSharedPreferences.edit();
                                    editor.putString("name","");
                                    editor.putString("userName","");
                                    editor.putString("token","");
                                    editor.apply();
                                    Intent i = new Intent(C2MOrder.this,MainActivity.class);
                                    startActivity(i);
                                    finish();
                                }

                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode,Header[] headers,Throwable tb,JSONObject response){
                            Log.e("gg", response.toString());

                        }
                    });


        } catch (JSONException e) {
            // TODO Auto-generated catch block
        }
    }
}
