package com.pmquiz.kwl.programmingquiz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

public class OS_Search extends AppCompatActivity {

    EditText search;
    ImageButton searchBtn;
    ProgressBar mprogressBar;
    Bundle b; Net_Con nc =new Net_Con();
    private SharedPreferences mSharedPreferences;
    public static final String myShared= "javaMCQ";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.os_search);

        mSharedPreferences = getSharedPreferences(myShared, Context.MODE_PRIVATE);

        mprogressBar =(ProgressBar) findViewById(R.id.simpleProgressBar);



        ActionBar bar = getSupportActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#DD2D11")));
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.main_action_bar);
        TextView tt=(TextView) findViewById(R.id.action_bar_title);
         b =getIntent().getExtras();
        if(b.getString("scan").isEmpty()){
            if(mSharedPreferences.getString("osHist","").isEmpty()){

            }else{
                mprogressBar.setVisibility(View.VISIBLE);

                if(nc.isNetworkConnected(OS_Search.this)==true){
                    getOslist(mSharedPreferences.getString("osHist",""));
                }else{
                    Toasty.error(OS_Search.this,"Please Turn on Internet",Toasty.LENGTH_LONG).show();
                }
            }
        }

        String scan ="";
        if(b.getString("scan").isEmpty()){
            tt.setText("Search Shop");
        }else if(b.getString("scan").equals("c2m")){
            tt.setText("C2M");
        }else{
            tt.setText("Track By ID");//req_track_by_id.php?keyword=1
        }


        search =(EditText) findViewById(R.id.searchText);
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mprogressBar.setVisibility(View.VISIBLE);
                SharedPreferences.Editor editor = mSharedPreferences.edit();
               // editor.putString(myShared_userName,user_id);
                if(b.getString("scan").isEmpty()){
                    editor.putString("osHist","");
                    editor.putString("osHist",s.toString());
                    editor.apply();
                }


                if(nc.isNetworkConnected(OS_Search.this)==true){
                    if(b.getString("scan").isEmpty()){
                        getOslist(search.getText().toString());
                    }else if(b.getString("scan").equals("c2m")){
                        getOslist(search.getText().toString());
                    }else{
                        if(s.length()>8){
                            getOrderById();//req_track_by_id.php?keyword=1
                        }

                    }
                }else{
                    Toasty.error(OS_Search.this,"Please Turn on Internet",Toasty.LENGTH_LONG).show();
                }



            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        searchBtn=(ImageButton) findViewById(R.id.searchBtn);
        searchBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if(nc.isNetworkConnected(OS_Search.this)==true ){
                    if(b.getString("scan").isEmpty()){
                        getOslist(search.getText().toString());
                    }else{
                        if(search.getText().toString().length()>8){
                            getOrderById();//req_track_by_id.php?keyword=1
                        }
                    }
                }else{
                    Toasty.error(OS_Search.this,"Please Turn on Internet",Toasty.LENGTH_LONG).show();
                }

            }
        });
    }

//    @Override
//    public void onBackPressed () {
//        this.finishAffinity();
//    }

    public void getOslist(String searchText) {
        AsyncHttpClient client = new AsyncHttpClient();
        JSONObject jsonParams=new JSONObject();
        try {
            if(searchText!=null){
                jsonParams.put("keyword", searchText);
            }else{
                jsonParams.put("keyword", "");
            }
//
           // jsonParams.put("last_call", "");
           // jsonParams.put("user", mSharedPreferences.getString("name", ""));
        }catch (JSONException e){

        }
        String url = "http://mobile.dailyexpress.com.mm/api/v1/req_search_shop";

        if(!b.getString("scan").isEmpty()&& b.getString("scan").equals("c2m")){
            url= "http://mobile.dailyexpress.com.mm/api/v1/search_c2m_user";
        }
        Log.e("search api", url+" "+jsonParams);
        HttpEntity entity =  new StringEntity(jsonParams.toString(),"UTF-8");
        client.addHeader("Authorization", "Bearer "+mSharedPreferences.getString("token", ""));
        client.post(getApplicationContext(),url, entity,"application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {


                try {
                    JSONObject serverResp = new JSONObject(response.toString());
                    mprogressBar.setVisibility(View.GONE);
                    if (serverResp.getInt("status")==200) {
                        Log.e("dhtae lrr",serverResp.getString("data"));
                        if(b.getString("scan").equals("c2m")){
                            getIntent().putExtra("scanId","c2m");
                        }else{
                            getIntent().putExtra("scanId","");
                        }

                        getIntent().putExtra("osResult","");
                        getIntent().putExtra("osResult",serverResp.getString("data") );
                        Log.e("testign1234",serverResp.getString("data"));
                         FragmentManager level_fragment= getSupportFragmentManager();
                         OSResultFragment osresult= new OSResultFragment();
                        FrameLayout searchFrame =(FrameLayout) findViewById(R.id.os_search_result) ;
                        searchFrame.removeAllViews();
                         level_fragment.beginTransaction().add(R.id.os_search_result,osresult).commit();

                    } else {
                        mprogressBar.setVisibility(View.GONE);
                        Toast.makeText(OS_Search.this, "Token expired Please Login Again ", Toast.LENGTH_LONG).show();
                        SharedPreferences.Editor editor = mSharedPreferences.edit();
                        editor.putString("name","");
                        editor.putString("userName","");
                        editor.putString("token","");
                        editor.apply();
                        Intent i = new Intent(OS_Search.this,MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String response, Throwable tb) {

            }

        });
    }

    public void getOrderById() {
        AsyncHttpClient client = new AsyncHttpClient();
        JSONObject jsonParams=new JSONObject();
        try {
          jsonParams.put("keyword", search.getText());
        }catch (JSONException e){

        }

        HttpEntity entity =  new StringEntity(jsonParams.toString(),"UTF-8");
        client.addHeader("Authorization", "Bearer "+mSharedPreferences.getString("token", ""));
        client.post(getApplicationContext(),"http://mobile.dailyexpress.com.mm/api/v1/req_track_by_id", entity,"application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {


                try {
                    JSONObject serverResp = new JSONObject(response.toString());
                    Log.e("osserach ",search.getText()+" "+ serverResp.getString("data"));
                    mprogressBar.setVisibility(View.GONE);
                    if (serverResp.getInt("status")==200) {
                        getIntent().putExtra("scanId","scan");
                        getIntent().putExtra("osResult","");
                        getIntent().putExtra("osResult",serverResp.getJSONObject("data").toString());
                        FragmentManager level_fragment= getSupportFragmentManager();
                        OSResultFragment osresult= new OSResultFragment();
                        FrameLayout searchFrame =(FrameLayout) findViewById(R.id.os_search_result) ;
                        searchFrame.removeAllViews();
                        level_fragment.beginTransaction().add(R.id.os_search_result,osresult).commit();

                    } else {
                        mprogressBar.setVisibility(View.GONE);
                        Toast.makeText(OS_Search.this, "Token expired Please Login Again ", Toast.LENGTH_LONG).show();
                        SharedPreferences.Editor editor = mSharedPreferences.edit();
                        editor.putString("name","");
                        editor.putString("userName","");
                        editor.putString("token","");
                        editor.apply();
                        Intent i = new Intent(OS_Search.this,MainActivity.class);
                        startActivity(i);
                        finish();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String response, Throwable tb) {

            }

        });
    }
}
