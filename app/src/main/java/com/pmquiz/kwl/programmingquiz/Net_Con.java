package com.pmquiz.kwl.programmingquiz;

import android.content.Context;
import android.net.ConnectivityManager;

import java.net.InetAddress;

public class Net_Con {

    public boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }


}
