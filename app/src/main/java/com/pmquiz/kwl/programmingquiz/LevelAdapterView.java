package com.pmquiz.kwl.programmingquiz;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class LevelAdapterView extends RecyclerView.Adapter<LevelAdapterView.ViewHolder>{
    private List<Level> levelList;
    private Context mContext;
    private OnItemClickListener mOnItemClickListener;
    private CardView levelBgCard;



    public interface OnItemClickListener {
        void onClick(View view, int position);
    }

    public LevelAdapterView(List<Level> list, Context context) {
        this.mContext = context;
        this.levelList = list;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.level_card_view, parent, false);

        return new ViewHolder(view);
    }


    public void onBindViewHolder(final ViewHolder holder, int position) {

        Level level = levelList.get(position);
        holder.subjectName.setText(level.getLevelname());

        holder.subjectName.setTypeface(Typeface.createFromAsset(holder.itemView.getContext().getAssets(),"Unicode.ttf"));
        holder.subjectName.setTextSize(15);
        holder.subjectIcon.setImageResource(level.getIcon_id());
        holder.cV.setCardBackgroundColor(Color.parseColor("#F0ECEC"));
        holder.cV.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                mOnItemClickListener.onClick(view, holder.getAdapterPosition());
            }
        });

    }

    public int getItemCount() {
        return levelList.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView subjectIcon;
        TextView subjectName;
        CardView cV;
        public ViewHolder(View itemView) {
            super(itemView);
            Typeface zawgyi= Typeface.createFromAsset(itemView.getContext().getAssets(),"Unicode.ttf");
            subjectIcon = (ImageView) itemView.findViewById(R.id.subject_image);
            subjectName = (TextView) itemView.findViewById(R.id.subject_name);
            cV=(CardView) itemView.findViewById(R.id.layout_card);
            Log.e("ind  ","ggg");
            subjectName.setTypeface(zawgyi);

        }
    }
}
