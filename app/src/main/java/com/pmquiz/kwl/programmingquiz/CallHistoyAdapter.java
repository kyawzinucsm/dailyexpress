package com.pmquiz.kwl.programmingquiz;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.List;

public class CallHistoyAdapter extends RecyclerView.Adapter<CallHistoyAdapter.ViewHolder> {
    private List<Level> levelList;
    private Context mContext;
    private CallHistoyAdapter.OnItemClickListener mOnItemClickListener;
    private CardView levelBgCard;


    public interface OnItemClickListener {
        void onClick(View view, int position);
    }

    public CallHistoyAdapter(List<Level> list, Context context) {
        this.mContext = context;
        this.levelList = list;
    }

    public CallHistoyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.e("in ther", "in cha");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.callhistory_card, parent, false);

        return new CallHistoyAdapter.ViewHolder(view);
    }


    public void onBindViewHolder(final CallHistoyAdapter.ViewHolder holder, int position) {
        Log.e("in call ","idfodf"+levelList);
        if(levelList!=null && levelList.size()>0){
            final Level level = levelList.get(position);
            holder.fileName.setText(level.getLevelname());
            holder.fileName.setTypeface(Typeface.DEFAULT_BOLD);
            holder.fileName.setTextSize(15);
            holder.cV.setCardBackgroundColor(Color.parseColor("#F0ECEC"));
            holder.cV.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    try {

                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        File file = new File(level.getLevelIcon());
                        if (file!=null)
                        {
                            intent.setDataAndType(FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID + ".provider",file), "audio/*");
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            Intent chooser = Intent.createChooser(intent, "Play the song with");
                            chooser.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            mContext.getApplicationContext().startActivity(chooser);
                        }

                    } catch(Exception e) {
                        Log.e("error", e.toString());
                        System.out.println(e.toString());
                    }
                }
            });
        }else{
            Log.e("no more ", "record");
            holder.fileName.setText("No Record");
            holder.fileName.setTypeface(Typeface.DEFAULT_BOLD);
            holder.fileName.setTextSize(15);
            holder.cV.setCardBackgroundColor(Color.parseColor("#F0ECEC"));
            holder.cV.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    try {
                        Toast.makeText(view.getContext(), "No Record exit",
                                Toast.LENGTH_LONG).show();
                    } catch(Exception e) {
                        System.out.println(e.toString());
                    }
                }
            });
        }


    }

    public int getItemCount() {
        return levelList.size();
    }

    public void setOnItemClickListener(CallHistoyAdapter.OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    private boolean isPackageInstalled(String packagename, PackageManager packageManager)
    {
        try
        {
            packageManager.getPackageInfo(packagename, 0);
            return true;
        }
        catch (PackageManager.NameNotFoundException e)
        {
            return false;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView fileName;
        CardView cV;

        public ViewHolder(View itemView) {
            super(itemView);
            fileName = (TextView) itemView.findViewById(R.id.fileName);
            cV=(CardView) itemView.findViewById(R.id.call_card);

        }
    }
}
