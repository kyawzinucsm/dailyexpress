package com.pmquiz.kwl.programmingquiz;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

public class RemarkActivity extends AppCompatActivity {

    EditText remarkText;
    TextInputLayout rmLabel;
    Button saveBtn;
    String id;
    ProgressBar pg;
    private SharedPreferences sp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.remark_activity);
        sp =getSharedPreferences("javaMCQ", Context.MODE_PRIVATE);
        Bundle b =getIntent().getExtras();
        id=b.getString("msgId");

        rmLabel=(TextInputLayout) findViewById(R.id.remark_layout);
        rmLabel.setHint("Error Remark");
        pg=(ProgressBar) findViewById(R.id.rmProgress);
        remarkText=(EditText) findViewById(R.id.remarkText);
        saveBtn =(Button) findViewById(R.id.save);
        saveBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                pg.setVisibility(View.VISIBLE);
                postData(remarkText.getText().toString(),id);
            }
        });

    }

    public void postData(String rmText,String orderId){
        JSONObject jsonParams = new JSONObject();
        AsyncHttpClient client = new AsyncHttpClient();
        try {
            // Add your data

            jsonParams.put("order_id", orderId);
            jsonParams.put("msg",rmText);
        }catch (JSONException e){
            Toasty.error(RemarkActivity.this, "Something wrong ",
                    Toast.LENGTH_LONG,true).show();
        }
        HttpEntity entity = new StringEntity(jsonParams.toString(),"UTF-8");
        client.addHeader("Authorization", "Bearer "+sp.getString("token", ""));
        client.post(getApplicationContext(),"http://mobile.dailyexpress.com.mm/api/v1/req_update_errormsg",entity,"application/json",new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                try {
                    Log.e("rem",response.toString());
                    if(response.getInt("status")==200){
                        pg.setVisibility(View.GONE);
                        JSONObject serverResp = new JSONObject(response.toString());
                        Toasty.success(RemarkActivity.this, serverResp.getString("msg"),
                                Toast.LENGTH_LONG,true).show();

                        Intent i =new Intent(RemarkActivity.this,HomePage.class);
                        startActivity(i);
                        finish();
                    }else{
                        Toast.makeText(RemarkActivity.this, "Token expired Please Login Again ", Toast.LENGTH_LONG).show();
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString("name","");
                        editor.putString("userName","");
                        editor.putString("token","");
                        editor.apply();
                        Intent i = new Intent(RemarkActivity.this,MainActivity.class);
                        startActivity(i);
                        finish();
                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toasty.success(RemarkActivity.this, e.getMessage(),
                            Toast.LENGTH_LONG,true).show();
                }
            }

            @Override
            public void onFailure(int statusCode,Header[] headers,String response,Throwable tb){
                ;
                Toasty.error(RemarkActivity.this, response,
                        Toast.LENGTH_LONG,true).show();
            }

        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent i = new Intent(RemarkActivity.this,HomePage.class);http://www.mobile.beedelivery.com.mm/api_v_1.0.0/req_update_errormsg.php?id=133944&msg=aa
//        startActivity(i);
        finish();
    }
}
