package com.pmquiz.kwl.programmingquiz;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.aykuttasil.callrecord.CallRecord;

import org.w3c.dom.Text;

import java.util.Date;
import java.util.List;

public class AssignAdapter extends RecyclerView.Adapter<AssignAdapter.ViewHolder>  {
    private List<OS_result> osList;
    static  String clickedState;
    private Context mContext;
    private OnItemClickListener mOnItemClickListener;
    private CardView levelBgCard;
    private OnLoadMoreListener onLoadMoreListener;
    private String userName;
    private String phNo;
    private CallRecord callRecord;
    private SharedPreferences sharedPreferences;


    public interface OnItemClickListener {
        void onClick(View view, int position);
    }

    public AssignAdapter(List<OS_result> list, Context context,String state) {
        this.mContext = context;
        this.osList = list;
        this.clickedState= state;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.assign_card, parent, false);

            return new ViewHolder(view);


    }


    public void onBindViewHolder(final ViewHolder holder, int position) {


            final OS_result osResult = osList.get(position);

        holder.userName.setText(" "+osResult.getUserName());
        holder.phNo.setText(" "+osResult.getPhoneNo());
        holder.userAdd.setText(" "+osResult.getUseraddress());
        holder.trackingCode.setText(" TrackingCode #"+osResult.getTrackingCode());
        holder.formatted_date.setText(osResult.getFormatted_date());
        holder.itemStatus.setText(" "+osResult.getItemStatus());
        holder.item_amount.setText(" "+osResult.getItemAmount()+" MMK");
            Log.e("holder postion",osList.get(position)+""+clickedState);
        holder.cv.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    mOnItemClickListener.onClick(view, holder.getAdapterPosition());
                }
            });
        holder.phCall.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                phNo= osResult.getPhoneNo();
                userName =osResult.getUserName();
                sharedPreferences = mContext.getSharedPreferences("javaMCQ",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                Log.e("checking order",osResult.getTrackingCode());
                editor.putString("recordNumber",phNo);
                editor.putString("recordOrder", osResult.getTrackingCode());
                editor.apply();
                Intent in = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+osResult.getPhoneNo()));
                try {
                    view.getContext().startActivity(in);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(view.getContext(), "Could not find an activity to place the call.", Toast.LENGTH_SHORT).show();
                }
            }
        });




    }

    public int getItemCount() {
        return osList.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        mOnItemClickListener = onItemClickListener;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView userName,phNo,userAdd,trackingCode,itemStatus,item_amount,formatted_date;
        Button phCall;
        CardView cv;



        public ViewHolder(View itemView)  {
            super(itemView);
            Typeface zawgyi= Typeface.createFromAsset(itemView.getContext().getAssets(),"Unicode.ttf");
            userName = (TextView) itemView.findViewById(R.id.user);
            phNo=(TextView) itemView.findViewById(R.id.ph);
            userAdd =(TextView) itemView.findViewById(R.id.add);
            formatted_date= (TextView) itemView.findViewById(R.id.formatted_date);
            trackingCode=(TextView) itemView.findViewById(R.id.trackCode);
            itemStatus = (TextView) itemView.findViewById(R.id.status);
            item_amount = (TextView) itemView.findViewById(R.id.amount);
            cv=(CardView) itemView.findViewById(R.id.assign_card_view);
            phCall = (Button) itemView.findViewById(R.id.phoneCall);
            Log.e("clciked", clickedState.equals("Assign")+"");
            if(clickedState.equals("Assign")){

                phCall.setVisibility(View.VISIBLE);
            }

            userName.setTypeface(zawgyi);
            phNo.setTypeface(zawgyi);
            userAdd.setTypeface(zawgyi);
            trackingCode.setTypeface(zawgyi);
            formatted_date.setTypeface(zawgyi);
            itemStatus.setTypeface(zawgyi);
            item_amount.setTypeface(zawgyi);



        }
    }

    private class ViewHolderLoading extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ViewHolderLoading(View view) {
            super(view);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);

        }
    }


}
