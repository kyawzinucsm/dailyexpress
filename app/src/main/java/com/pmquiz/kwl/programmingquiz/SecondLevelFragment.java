package com.pmquiz.kwl.programmingquiz;


import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class SecondLevelFragment extends Fragment {

    private final static String MIPMAP = "mipmap";

    private RecyclerView mRecycleView;
    private Activity mActivity;
    private CardView layout_card;
    BottomNavigationView bottomNavigationView;

    public SecondLevelFragment() {

    }

    public static LevelFragment newInstance() {
        LevelFragment fragment = new LevelFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mActivity = getActivity();
        mRecycleView = (RecyclerView) view.findViewById(R.id.secondlevel_fragment);
        bottomNavigationView=(BottomNavigationView) getActivity().findViewById(R.id.bottom_navigation);
        mRecycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {



            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {

                if (dy > 0 && bottomNavigationView.isShown()) {
                    bottomNavigationView.setVisibility(View.GONE);
                } else if (dy < 0 ) {
                    bottomNavigationView.setVisibility(View.VISIBLE);

                }



            }
        });
        setLevelView();
    }

    private void setLevelView() {
        List<Level> levelList= new ArrayList<>();
        List<String> levelName =new ArrayList<String>();
        levelName.add("GC");
        levelName.add("Online Shop");
        levelName.add("C2MOrder");
        levelName.add("Calculator");
        levelName.add("Delivering");
        levelName.add("Delivered");
        levelName.add("Assign");

        for(int j=0;j<levelName.size();j++){
            Level level =new Level();
            level.setLevelname(levelName.get(j));
            levelList.add(level);
        }
        loadData(levelList);
        SecondLevelAdapter levelAdapterView = new SecondLevelAdapter(levelList, getContext());
        setOnclickListener(levelAdapterView, levelList);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mActivity, 2);
        mRecycleView.setLayoutManager(gridLayoutManager);
        mRecycleView.setAdapter(levelAdapterView);
    }

    private void loadData(List<Level> levelList) {
        Resources resources = mActivity.getResources();
        String packageName = mActivity.getPackageName();
        for(Level level:levelList) {
            String iconName = level.getLevelname().toLowerCase();
            int icon=0;
            String os="onlineshop";

            if(level.getLevelname().toLowerCase().replaceAll("\\s+","").equals(os)){
                icon = getResourceByName("os","drawable", resources, packageName);

            }else if(level.getLevelname().toLowerCase().replaceAll("\\s+","").equals("delivering")){
                icon = getResourceByName("deliveringlist","drawable", resources, packageName);
            }else if(level.getLevelname().toLowerCase().replaceAll("\\s+","").equals("delivered")){
                icon = getResourceByName("deliveredlist","drawable", resources, packageName);
            }else if(level.getLevelname().toLowerCase().replaceAll("\\s+","").equals("calculator")){
                icon = getResourceByName("de","drawable", resources, packageName);
            }else if(level.getLevelname().toLowerCase().replaceAll("\\s+","").equals("c2morder")){
                icon = getResourceByName("gc","drawable", resources, packageName);
            }else{
                icon = getResourceByName(iconName,"drawable", resources, packageName);
                level.setIcon_id(icon);
            }

            level.setIcon_id(icon);
        }
    }

    private int getResourceByName(String name, String identifier, Resources resources, String packageName) {
        return resources.getIdentifier(name, identifier, packageName);
    }

    private void setOnclickListener(final SecondLevelAdapter levelAdapterView, final List<Level> levelList) {
        levelAdapterView.setOnItemClickListener(new SecondLevelAdapter.OnItemClickListener() {

            @Override
            public void onClick(View view, int position) {
                Log.e("testing ","onclick leveladapter"+levelList.get(position).getLevelname());
                Intent i ;
                if(position==0){
                    i= new Intent(mActivity, CreateOrder.class);
                    startActivity(i);
                }else if(position==1){
                    i= new Intent(mActivity, OS_Search.class);
                    i.putExtra("scan","");
                    startActivity(i);
                }else if(position==3){
                    i = new Intent(mActivity,WebViewCalculator.class);
                    startActivity(i);
                }else if(position==2){
                    i= new Intent(mActivity, OS_Search.class);
                    i.putExtra("scan","c2m");
                    startActivity(i);
                }else{
                    i= new Intent(mActivity, Assign.class);
                    i.putExtra("itemState",levelList.get(position).getLevelname());
                    startActivity(i);
                }


            }
        });
    }
}
