package com.pmquiz.kwl.programmingquiz;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.os.Vibrator;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.karan.churi.PermissionManager.PermissionManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

public class OrderDetail extends AppCompatActivity {



   TextView qrCode,userName,phNo, r_name,r_phNo,r_Address,item_name,fee,typeOfde,totalAmount,remark;
   Button ok_button,collect_button;
    OS_result json;
    ProgressBar pg;

    private SharedPreferences mSharedPreferences;
    public static final String myShared= "javaMCQ";
    public static final String myShared_userName = "name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_detail);
        pg=(ProgressBar) findViewById(R.id.collectProgress);

        mSharedPreferences = getSharedPreferences(myShared, Context.MODE_PRIVATE);

        ActionBar bar =getSupportActionBar();
        bar.setTitle("Order Detail");
        Intent i = getIntent();
        json =(OS_result) i.getSerializableExtra("order");
        String ggez =i.getStringExtra("createOrder");
        if(ggez!=null){
            ok_button =(Button) findViewById(R.id.ok);
            collect_button =(Button) findViewById(R.id.pk);

            ok_button.setVisibility(View.VISIBLE);
            collect_button.setVisibility(View.VISIBLE);
            ok_button.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                   Intent i = new Intent(OrderDetail.this,HomePage.class);
                   i.putExtra("od","od");
                   startActivity(i);
                   finish();
                }
            });
            collect_button.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    Net_Con nc =new Net_Con();
                    if(nc.isNetworkConnected(OrderDetail.this)==true ){
                        pg.setVisibility(View.VISIBLE);
                    }else{
                        Toasty.error(OrderDetail.this,"Please Turn on Internet",Toasty.LENGTH_LONG).show();
                    }

                    sendPickup(json.getItem_id());
                }
            });
        }

       // OS_result r=(OS_result) json;
        Log.e("json order",json.getUserName()+"");
        Typeface zawgyi= Typeface.createFromAsset(getApplicationContext().getAssets(),"Unicode.ttf");
        qrCode=(TextView) findViewById(R.id.qrCode);
        userName=(TextView) findViewById(R.id.userName);
        phNo=(TextView) findViewById(R.id.phNo);
        r_name=(TextView) findViewById(R.id.r_name);
        r_phNo=(TextView) findViewById(R.id.r_phNo);
        r_Address=(TextView) findViewById(R.id.r_Address);
        item_name=(TextView) findViewById(R.id.item_name);
        fee=(TextView) findViewById(R.id.fee);
        typeOfde=(TextView) findViewById(R.id.typeOfde);
        totalAmount=(TextView) findViewById(R.id.totalAmount);
        remark=(TextView) findViewById(R.id.remark);

        qrCode.setTypeface(zawgyi);
        qrCode.setText(json.getTrackingCode());

        userName.setTypeface(zawgyi);
        userName.setText(json.getS_Name());

        phNo.setTypeface(zawgyi);
        phNo.setText(json.getS_Phone());

        r_name.setTypeface(zawgyi);
        r_name.setText(json.getUserName());

        r_phNo.setTypeface(zawgyi);
        r_phNo.setText(json.getPhoneNo());

        r_Address.setTypeface(zawgyi);
        r_Address.setText(json.getUseraddress());

        item_name.setTypeface(zawgyi);
        item_name.setText(json.getItemName());

        fee.setTypeface(zawgyi);
        fee.setText(json.getItemCod());

        typeOfde.setTypeface(zawgyi);
        typeOfde.setText(json.getDcType());

        totalAmount.setTypeface(zawgyi);
        totalAmount.setText(json.getItemAmount());

        remark.setTypeface(zawgyi);
        remark.setText(json.getRemark_item());






    }

    public void sendPickup(String order_id) {

        JSONObject jsonParams = new JSONObject();
        String userId="" ;if(mSharedPreferences.contains("name")){
            userId =mSharedPreferences.getString(myShared_userName,"");
        }
        AsyncHttpClient client = new AsyncHttpClient();
        try{
            jsonParams.put("user", userId);
            jsonParams.put("pk", order_id);
        }catch (JSONException e){

        }


        HttpEntity entity = new StringEntity(jsonParams.toString(),"UTF-8");
        client.addHeader("Authorization", "Bearer "+mSharedPreferences.getString("token", ""));
        client.post(getApplicationContext(),"http://mobile.dailyexpress.com.mm/api/v1/req_set_pickup_user", entity,"application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // If the response is JSONObject instead of expected JSONArrayhttp://www.mobile.beedelivery.com.mm/api_v_1.0.0/req_cities.php?param=no_param

                try {
                    JSONObject serverResp = new JSONObject(response.toString());
                    pg.setVisibility(View.GONE);

                    if (serverResp.getInt("status")==200) {
                        Toasty.success(OrderDetail.this, serverResp.getString("msg"),
                                Toast.LENGTH_LONG,true).show();
                        Intent i = new Intent(OrderDetail.this, HomePage.class);
                        i.putExtra("od","od");
                        startActivity(i);
                        finish();
                    } else {
                        Toast.makeText(OrderDetail.this, "Token expired Please Login Again ", Toast.LENGTH_LONG).show();
                        SharedPreferences.Editor editor = mSharedPreferences.edit();
                        editor.putString("name","");
                        editor.putString("userName","");
                        editor.putString("token","");
                        editor.apply();
                        Intent j= new Intent(OrderDetail.this,MainActivity.class);
                        startActivity(j);
                        finish();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String response, Throwable tb) {

            }

        });
    }

}


