package com.pmquiz.kwl.programmingquiz;

public interface OnLoadMoreListener {
    void onLoadMore();
}
